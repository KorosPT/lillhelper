﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{
    /// <summary>
    /// Class containing one or more IHelper classes
    /// </summary>
    public interface IKeyManaging : IManaging
    {
        /// <summary>
        /// Returns an array of IHelping (class that reacts to a key press)
        /// </summary>
        /// <returns></returns>
        IHelping[] getHelpers();

        System.Windows.Forms.MenuItem[] getMenuButtons();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{
    public interface IManaging
    {
        /// <summary>
        /// Get short fancy name
        /// </summary>
        /// <returns></returns>
        String getBriefDescription();

        /// <summary>
        /// Get control anel with settings
        /// </summary>
        /// <returns></returns>
        System.Windows.Forms.UserControl getControlPanel();

        string ToString();
    }
}

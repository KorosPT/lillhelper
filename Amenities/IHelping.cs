﻿using System.Collections.Generic;

namespace Amenities
{
    /// <summary>
    /// Interface for any class that is to be registered as a Helper (reacts to some key strokes with some action)
    /// </summary>
    public interface IHelping
    {

        /// <summary>
        /// Returns the binding to which the Helper wants to react
        /// </summary>
        /// <returns> the key binding</returns>
        KeyCombo[] getKeyBinding();

        /// <summary>
        /// Should the key press be suppresed (Ex: on ctrl+c there will not be a 'c' writen)
        /// </summary>
        /// <returns>Should be suppressed</returns>
        bool suppressKey(LinkedListNode<KeyCombo> pressedKey);

        /// <summary>
        /// Do what you are here to do
        /// </summary>
        /// <returns>Ended succesfully</returns>
        bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKey);

        /// <summary>
        /// Dispose of everythinf that could stop propper exitting of servisce
        /// </summary>
        void Dispose();
    
        bool isEnabled();

        void setEnabled(bool enabled);

        bool playNotification();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{

    /// <summary>
    /// Core Helper class. 
    /// Manages .toString(), comparing and triggering
    /// </summary>
    public abstract  class Helper
    {
        //Combination to which the Helper reacts
        protected KeyCombo[] keyBinding;

        public KeyCombo[] getKeyBinding()
        {
            return keyBinding;
        }

        /// <summary>
        /// returns the name of Helper and kombination it responds to
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            String output = this.GetType().Name + " -";

            foreach (KeyCombo combo in keyBinding)
                output += " " + combo.ToString() + " &&";
              
            output = output.Substring(0, output.Length - 3);
            return output;
        }

        /// <summary>
        /// Checks if the combination Helper responds to is among recently pressed keys
        /// </summary>
        /// <param name="recentlyPressedKey"></param>
        /// <returns></returns>
        protected bool combinationTriggered(LinkedListNode<KeyCombo> recentlyPressedKey)
        {
            LinkedListNode<KeyCombo> currentNode = recentlyPressedKey;

            for (int i = keyBinding.Length - 1; i >= 0; i--)
            {
                if (currentNode == null)
                    return false;

                if (keyBinding[i].Equals(currentNode.Value))
                {

                    currentNode = currentNode.Previous;
                }
                else
                    return false;
            }
            return true;
        }


        public virtual void Dispose()
        { }

        public virtual bool playNotification()
        {
            return true;
        }

        public virtual bool suppressKey(LinkedListNode<KeyCombo> pressedKey)
        {
            if (isEnabled())
                return combinationTriggered(pressedKey);
            else
                return false;
        }

        bool enabled = true;
        public bool isEnabled()
            {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{
    /// <summary>
    /// Settings of the application
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Keyboard hook turned on
        /// </summary>
        public static bool HookOn { get; set; }

        /// <summary>
        /// Timer running
        /// Stops only countdowns. Alarms still run if TimerOn == false
        /// </summary>
        public static bool TimerOn { get; set; }

        /// <summary>
        /// Application wide order to shut down
        /// </summary>
        public static bool ShutingDown { get; set; }

        /// <summary>
        /// Tick period of the timer
        /// Life expectancy of KeyCombo
        /// </summary>
        public static long tickPeriod = 1000;




        static string settingsFileName = "LillSettings.txt";
        static char settingSeparator = '^';
        static Dictionary<string, string> settingsValues;


        public static bool loadSettings()
        {
            return loadSettings(true);
        }
        
        /// <summary>
        /// Load settings from a file
        /// </summary>
        /// <param name="cleanLoad">Disregard all loaded settings and load new ones</param>
        /// <returns></returns>
        public static bool loadSettings(bool cleanLoad)
        {
            if(cleanLoad || settingsValues == null)
                settingsValues = new Dictionary<string, string>();

            string filePath = getSettingsFilePath();
            if (!File.Exists(filePath))
                saveSettings(false);

            string line;
            System.IO.StreamReader file =  new System.IO.StreamReader(filePath);
            while ((line = file.ReadLine()) != null)
            {
                string[] tokenized = line.Split(settingSeparator);
                tokenized = tokenized.Select(innerItem => innerItem != null ? innerItem.Trim() : null).ToArray(); //Trim all
                if (tokenized.Length != 2)
                    continue;

                if(cleanLoad || (!cleanLoad && !settingsValues.ContainsKey(tokenized[0])) )
                    settingsValues.Add(tokenized[0], tokenized[1]);
            }
            file.Close();
            
            return true;
        }

        public static void saveSettings()
        {
            saveSettings(true);
        }
        public static void saveSettings(bool fileFound)
        {
            if(fileFound)
                loadSettings(false);

            string filePath = getSettingsFilePath();

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath, false))
            {
                foreach(KeyValuePair<string,string> pair in settingsValues)
                {
                    file.WriteLine(String.Format("{0} {1} {2}",pair.Key , settingSeparator,pair.Value));
                }
            }
        }

        public static string getSettingValue(string name, string defValue)
        {
            if (!settingsValues.ContainsKey(name))
                settingsValues.Add(name.Trim(), defValue.Trim());

            return settingsValues[name];
        }

        public static int getSettingValueInt(string name, int defValue)
        {
            if (!settingsValues.ContainsKey(name))
                settingsValues.Add(name.Trim(), defValue.ToString());

            int value = -1;
            if (Int32.TryParse(settingsValues[name], out value))
                return value;

            return defValue;
        }

        public static bool getSettingValueBool(string name, bool defValue)
        {
            if (!settingsValues.ContainsKey(name))
                settingsValues.Add(name.Trim(), defValue.ToString());

            bool value = defValue;
            if (Boolean.TryParse(settingsValues[name], out value))
                return value;

            return defValue;
        }

        public static void setSettingValue(string name, string value)
        {
            if (settingsValues.ContainsKey(name))
                settingsValues[name] = value;
            else
                settingsValues.Add(name, value);
        }

        public static void setSettingValueInt(string name, int value)
        {
            if (settingsValues.ContainsKey(name))
                settingsValues[name] = value.ToString();
            else
                settingsValues.Add(name, value.ToString());
        }

        public static void setSettingValueBool(string name, bool value)
        {
            if (settingsValues.ContainsKey(name))
                settingsValues[name] = value.ToString();
            else
                settingsValues.Add(name, value.ToString());
        }

        private static string getSettingsFilePath()
        {
            string filePath = getInstallationPath() + "\\" + settingsFileName;
            return filePath;
        }

        public static string getInstallationPath()
        {
            string filePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            filePath = filePath.Substring(0, filePath.LastIndexOf("\\")) + "\\";
            return filePath;
        }
    }
}

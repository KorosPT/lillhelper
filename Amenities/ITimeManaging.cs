﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{
    /// <summary>
    /// Class containing one or more ITiming classes
    /// </summary>
    public interface ITimeManaging : IManaging
    {
        /// <summary>
        /// Returns an array of ITiming (class that reacts to a progrssion of time)
        /// </summary>
        /// <returns></returns>
        ITiming[] getTimers();


        System.Windows.Forms.MenuItem[] getMenuButtons();
    }
}

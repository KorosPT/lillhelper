﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amenities
{
    /// <summary>
    /// Interface for any class that is to be registered as a Helper (reacts to passage of time)
    /// </summary>
    public interface ITiming
    {
        /// <summary>
        /// Time has come to check the time
        /// </summary>
        void trigger();

        /// <summary>
        /// Is timer counting down to something?
        /// </summary>
        bool isPeriodical();

        bool isEnabled();

        void setEnabled(bool enabled);

    }
}

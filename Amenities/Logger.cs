﻿
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amenities
{
    /// <summary>
    /// Somewhat stupidly created logger class (I guess... just a wrapper TBH)
    /// </summary>
    public static class Logger
    {
        public static ILog Log { get; set; }
        private static Semaphore semaphore = new Semaphore(1, 1);

        public static void LogInfo(String text)
        {
            semaphore.WaitOne();

            Log.Info(text);

            semaphore.Release();
        }

        public static void LogError(String text)
        {
            semaphore.WaitOne();

            Log.Error(text);

            semaphore.Release();
        }

        public static void LogWarn(String text)
        {
            semaphore.WaitOne();

            Log.Warn(text);

            semaphore.Release();
        }
    }
}

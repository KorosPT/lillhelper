﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amenities
{
    /// <summary>
    /// Wrapper class for KeyEventArgs
    /// </summary>
    public class KeyCombo : KeyEventArgs
    {
        /// <summary>
        /// Times the existence of KeyCombo
        /// </summary>
        Stopwatch expirationTimer;

        /// <summary>
        /// If true, the KeyCombo is not a triggering combination and interupts any sequence
        /// </summary>
        private bool sequenceBreak = false;

        /// <summary>
        /// Construtor for creating brand new instances just for comparing
        /// </summary>
        /// <param name="keyData"></param>
        public KeyCombo(Keys keyData) : base(keyData)
        {
        }

        /// <summary>
        /// Constructor used for converting from KeyEventArgs
        /// </summary>
        /// <param name="keyArgs"></param>
        public KeyCombo(KeyEventArgs keyArgs) : base(keyArgs.KeyData)
        {
        }

        public override bool Equals(object obj)
        {
            KeyCombo key2 = obj as KeyCombo;
            return key2.Control == this.Control && key2.Shift == this.Shift && key2.Alt == this.Alt && this.KeyCode == key2.KeyCode;
        }

        public override int GetHashCode()
        {
            return new { Control, Shift, Alt, KeyCode }.GetHashCode();
        }

        public override string ToString()
        {
            String output = "";

            if (this.Control)
                output += "Ctrl + ";
            if (this.Shift)
                output += "Shift + ";
            if (this.Alt)
                output += "Alt + ";

            if (this.KeyValue > 60 && this.KeyValue < 95)
                output += this.KeyCode;
            else if (output.Length > 3)
                output = output.Substring(0, output.Length - 3);

            return output;
        }

        /// <summary>
        /// Checks if the KeyCombo is old enught to be forgotten
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public bool TimedOut(int timeout)
        {
            if (expirationTimer.Elapsed.TotalMilliseconds > timeout)
            {
                expirationTimer.Stop();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Starts the expiration timer
        /// </summary>
        public void StartCuntDown()
        {
            if (expirationTimer == null)
                expirationTimer = new Stopwatch();

            if (!expirationTimer.IsRunning)
                expirationTimer.Start();
        }

        public void SetSequenceBreak(bool value)
        {
            sequenceBreak = value;
        }

        public bool IsSequenceBreak()
        {
            return sequenceBreak;
        }
    }
}

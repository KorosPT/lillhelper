﻿using Amenities;
using Common.Logging;
using CoreFunctionality;
using Gma.System.MouseKeyHook;
using System;
using System.Threading;
using System.Windows.Forms;
using Topshelf;

namespace LillHelper.Service
{
    class WinService
    {
        public static ILog Log { get; private set; }

        public Thread coreThread;
        public CoreDispatcher core;
        AutoResetEvent coreAlarmclock;

        private IKeyboardMouseEvents m_Events;

        public WinService(ILog logger)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            Log = logger;
            Logger.Log = logger;
        }

        public void StartCore()
        {
            Log.Info($"{nameof(Service.WinService)} Core dispatcher starting.");

            coreAlarmclock = new AutoResetEvent(false);

            core = new CoreDispatcher(coreAlarmclock);
            core.Run();
        }


        public bool Start(HostControl hostControl)
        {
            Log.Info($"{nameof(Service.WinService)} Start command received.");

            //Starting the core service handling
            coreThread = new Thread(new ThreadStart(StartCore));
            coreThread.IsBackground = true;
            coreThread.Start();

            //Starting the keyboard hook
            SubscribeGlobal();
            Application.Run();

            return true;
        }
        

        public bool Stop(HostControl hostControl)
        {

            Log.Trace($"{nameof(Service.WinService)} Stop command received.");

            //Stoping the Thread
            core.keepRunning = false;
            coreAlarmclock.Set();
            coreAlarmclock.Close();
            coreThread.Join();

            //Stoping the hook
            Application.Exit();
            Unsubscribe();

            return true;
        }

        private void SubscribeGlobal()
        {
            Unsubscribe();
            Subscribe(Hook.GlobalEvents());
        }

        private void Subscribe(IKeyboardMouseEvents events)
        {
            m_Events = events;

            m_Events.KeyDown += OnKeyDown;
        }

        private void Unsubscribe()
        {
            if (m_Events == null) return;
            m_Events.KeyDown -= OnKeyDown;

            m_Events.Dispose();
            m_Events = null;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            KeyCombo keyCombo = new KeyCombo(e);

            Tuple<bool, bool> result = core.director.KeyPress(keyCombo);

            if(result.Item1)
                e.SuppressKeyPress = true;

            Log.Info(string.Format("KeyDown \t {0}", keyCombo.ToString()+"\tProcessing:\t"+result.Item1.ToString()));

            if (result.Item2)
                coreAlarmclock.Set();
        }


        public bool Pause(HostControl hostControl)
        {

            Log.Trace($"{nameof(Service.WinService)} Pause command received.");

            //TODO: Implement your service start routine.
            return true;

        }

        public bool Continue(HostControl hostControl)
        {

            Log.Trace($"{nameof(Service.WinService)} Continue command received.");

            //TODO: Implement your service stop routine.
            return true;

        }

        public bool Shutdown(HostControl hostControl)
        {

            Log.Trace($"{nameof(Service.WinService)} Shutdown command received.");

            //TODO: Implement your service stop routine.
            return true;

        }

    }
}

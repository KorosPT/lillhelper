﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    /// <summary>
    /// Simple JapIn experiment. On Ctrl + U writes ů
    /// </summary>
    class Experiment1 : Helper, IHelping
    {

        public Experiment1()
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.OemMinus), new KeyCombo(Keys.U) };
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(Experiment1)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(Experiment1)} Combination doesn't match.");
                return false;
            }

            //System.Threading.Thread.Sleep(50); //Sleep so the writing is not canceled by user still holding Ctrl
            SendKeys.SendWait("ů");

            return true;
        }

        public override bool playNotification()
        {
            return false;
        }
    }
}

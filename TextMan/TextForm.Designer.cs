﻿namespace TextMan
{
    partial class TextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_noteText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_noteText
            // 
            this.txt_noteText.Location = new System.Drawing.Point(9, 10);
            this.txt_noteText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_noteText.Multiline = true;
            this.txt_noteText.Name = "txt_noteText";
            this.txt_noteText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_noteText.Size = new System.Drawing.Size(270, 66);
            this.txt_noteText.TabIndex = 1;
            this.txt_noteText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            // 
            // TextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 82);
            this.Controls.Add(this.txt_noteText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TextForm";
            this.Text = "Random line";
            this.Shown += new System.EventHandler(this.TextForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txt_noteText;
    }
}
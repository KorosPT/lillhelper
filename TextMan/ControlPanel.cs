﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    public partial class ControlPanel : UserControl
    {
        TextMan textMan;

        public ControlPanel(TextMan textMan)
        {
            InitializeComponent();
            this.textMan = textMan;
            chck_Exp1.Checked = textMan.getHelpers()[0].isEnabled();
        }

        private void chck_Exp1_CheckedChanged(object sender, EventArgs e)
        {
            textMan.getHelpers()[0].setEnabled(chck_Exp1.Checked);
            textMan.saveSoundSettings();
        }
    }
}

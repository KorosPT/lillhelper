﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    class RandLine : Helper, IHelping
    {
        public static TextForm form;

        public RandLine()
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.C), new KeyCombo(Keys.Control | Keys.Alt | Keys.R) };
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(RandLine)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(RandLine)} Combination doesn't match.");
                return false;
            }

            //Get text i clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }
                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            string[] lines = clipText.Split('\n');

            Random rnd = new Random();
            int line = (int)(rnd.NextDouble() * lines.Length);

            form = new TextForm(lines[line]);
            form.ShowDialog();

            return true;
        }
    }
}

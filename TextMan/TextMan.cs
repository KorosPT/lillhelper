﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    public class TextMan : IKeyManaging
    {
        private Experiment1 exp1;
        private RandLine rand;
        private Stats stats;

        private string exp1_enabled = "TEXTMAN_EXP1_ENABLED";

        public TextMan()
        {
            exp1 = new Experiment1();
            rand = new RandLine();
            stats = new Stats();

            loadSettings();
        }

        private void loadSettings()
        {
            bool exp1enabled = Settings.getSettingValueBool(exp1_enabled, true);

            exp1.setEnabled(exp1enabled);
        }

        internal void saveSoundSettings()
        {
            Settings.setSettingValueBool(exp1_enabled, exp1.isEnabled());
            Settings.saveSettings();
        }

        public String getBriefDescription()
        {
            return "Text manager";
        }

        public System.Windows.Forms.UserControl getControlPanel()
        {
            return new ControlPanel(this);
        }

        public override string ToString()
        {
            return getBriefDescription();
        }

        public IHelping[] getHelpers()
        {
            return new IHelping[] { exp1, rand, stats };
        }

        public MenuItem[] getMenuButtons()
        {
            return new MenuItem[0];
        }
    }
}

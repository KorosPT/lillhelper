﻿using System.Threading.Tasks;
using System.Windows.Forms;
using TextMan.Properties;

namespace TextMan
{

    /// <summary>
    /// Detail of one note
    /// </summary>
    partial class TextForm : Form
    {
        string note;
        public TextForm(string note)
        {
            this.note = note;
            InitializeComponent();
            this.Icon = Resources.Icon1;

            showData(note);
        }

        private void showData(string note)
        {
            txt_noteText.Text = note;
            txt_noteText.Select(0, 0);
        }

        private void NoteDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void TextForm_Shown(object sender, System.EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
    }
}

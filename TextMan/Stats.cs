﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    class Stats : Helper, IHelping
    {
        public Stats()
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.C), new KeyCombo(Keys.Control | Keys.Alt | Keys.S) };
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(Stats)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(Stats)} Combination doesn't match.");
                return false;
            }

            //Get text i clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }
                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            StatForm  form = new StatForm(clipText);
            form.ShowDialog();

            return true;
        }
    }
}

﻿using TextMan.Properties;

namespace TextMan
{
    partial class StatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wordsNum = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sumNum = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linesNum = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // wordsNum
            // 
            this.wordsNum.AutoSize = true;
            this.wordsNum.Location = new System.Drawing.Point(102, 28);
            this.wordsNum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.wordsNum.Name = "wordsNum";
            this.wordsNum.Size = new System.Drawing.Size(24, 13);
            this.wordsNum.TabIndex = 4;
            this.wordsNum.Text = "n/a";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Words:";
            // 
            // sumNum
            // 
            this.sumNum.AutoSize = true;
            this.sumNum.Location = new System.Drawing.Point(102, 88);
            this.sumNum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sumNum.Name = "sumNum";
            this.sumNum.Size = new System.Drawing.Size(24, 13);
            this.sumNum.TabIndex = 6;
            this.sumNum.Text = "n/a";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 88);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Sum:";
            // 
            // linesNum
            // 
            this.linesNum.AutoSize = true;
            this.linesNum.Location = new System.Drawing.Point(102, 58);
            this.linesNum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linesNum.Name = "linesNum";
            this.linesNum.Size = new System.Drawing.Size(24, 13);
            this.linesNum.TabIndex = 8;
            this.linesNum.Text = "n/a";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 58);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Lines:";
            // 
            // StatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 253);
            this.Controls.Add(this.linesNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sumNum);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wordsNum);
            this.Controls.Add(this.label1);
            this.Icon = global::TextMan.Properties.Resources.Icon1;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "StatForm";
            this.Text = "StatForm";
            this.Shown += new System.EventHandler(this.StatForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StatForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label wordsNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label sumNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label linesNum;
        private System.Windows.Forms.Label label6;
    }
}
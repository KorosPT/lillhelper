﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextMan
{
    public partial class StatForm : Form
    {
        string text;
        public StatForm(string text)
        { 
            this.text = text.Replace('\r',' ');
            InitializeComponent();
            loadValues();
        }

        private void loadValues()
        {
            string[] lines = text.Split('\n');
            string[] words = text.Replace('\r', ' ').Replace('\n', ' ').Split(' ');
            linesNum.Text = lines.Length.ToString();
            wordsNum.Text = words.Length.ToString();
            tryAndSum(words);
        }

        private void tryAndSum(string[] words)
        {
            Double sum = 0;
            Double num = 0;
            foreach(string word in words)
            {
                if (string.IsNullOrEmpty(word)) continue;
                if(Double.TryParse(word, out num))
                {
                    sum += num;
                }
                else
                    return;
            }

            sumNum.Text = sum.ToString();
        }

        private void StatForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void StatForm_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
    }
}

﻿namespace TextMan
{
    partial class ControlPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chck_Exp1 = new System.Windows.Forms.CheckBox();
            this.chck_Stats = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chck_Exp1
            // 
            this.chck_Exp1.AutoSize = true;
            this.chck_Exp1.Location = new System.Drawing.Point(14, 15);
            this.chck_Exp1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chck_Exp1.Name = "chck_Exp1";
            this.chck_Exp1.Size = new System.Drawing.Size(84, 17);
            this.chck_Exp1.TabIndex = 0;
            this.chck_Exp1.Text = "Experiment1";
            this.chck_Exp1.UseVisualStyleBackColor = true;
            this.chck_Exp1.CheckedChanged += new System.EventHandler(this.chck_Exp1_CheckedChanged);
            // 
            // chck_Stats
            // 
            this.chck_Stats.AutoSize = true;
            this.chck_Stats.Location = new System.Drawing.Point(14, 37);
            this.chck_Stats.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chck_Stats.Name = "chck_Stats";
            this.chck_Stats.Size = new System.Drawing.Size(80, 17);
            this.chck_Stats.TabIndex = 1;
            this.chck_Stats.Text = "checkBox2";
            this.chck_Stats.UseVisualStyleBackColor = true;
            this.chck_Stats.Visible = false;
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chck_Stats);
            this.Controls.Add(this.chck_Exp1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ControlPanel";
            this.Size = new System.Drawing.Size(350, 228);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chck_Exp1;
        private System.Windows.Forms.CheckBox chck_Stats;
    }
}

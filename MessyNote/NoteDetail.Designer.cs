﻿namespace MessyNote
{
    partial class NoteDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_date = new System.Windows.Forms.Label();
            this.txt_noteText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(13, 13);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(46, 17);
            this.lbl_date.TabIndex = 0;
            this.lbl_date.Text = "label1";
            // 
            // txt_noteText
            // 
            this.txt_noteText.Location = new System.Drawing.Point(16, 46);
            this.txt_noteText.Multiline = true;
            this.txt_noteText.Name = "txt_noteText";
            this.txt_noteText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_noteText.Size = new System.Drawing.Size(313, 150);
            this.txt_noteText.TabIndex = 1;
            this.txt_noteText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            // 
            // NoteDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 228);
            this.Controls.Add(this.txt_noteText);
            this.Controls.Add(this.lbl_date);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NoteDetail";
            this.Text = "NoteDetail";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.TextBox txt_noteText;
    }
}
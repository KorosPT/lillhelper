﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote
{
    /// <summary>
    /// Key manager MassyNotes has two IHelping classes
    /// NoteWriter saves text in clipboard into database
    /// NoteReader shows list of notes in database
    /// </summary>
    public class MessyNote : IKeyManaging
    {
        private NoteWriter writer;
        private NoteReader reader;
        private static NotesContext context;

        public MessyNote()
        {
            context = new NotesContext();
            writer = new NoteWriter(context);
            reader = new NoteReader(context);
        }

        public IHelping[] getHelpers()
        {
            return new IHelping[]{ writer, reader };
        }

        public String getBriefDescription()
        {
            return "Note taking";
        }

        public UserControl getControlPanel()
        {
            return new ControlPanel(this);
        }

        public override string ToString()
        {
            return getBriefDescription();
        }

        static internal void recreateCache()
        {
            Cursor.Current = Cursors.WaitCursor;
            context.WaitForMe();
            XmlManager.WriteCache(XmlManager.getChachePath(), context);
            XmlManager.ReadChache(XmlManager.getChachePath());
            Cursor.Current = Cursors.Default;
        }

        public MenuItem[] getMenuButtons()
        {
            return new MenuItem[0];
        }
    }
}

﻿using Amenities;
using MessyNote.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote
{
    /// <summary>
    /// Form dosplaying notes
    /// </summary>
    partial class NoteLister : Form
    {
        NotesContext context;
        object selectedNoteID = null;
        private bool showingTrash = false;
        public bool RefreshCacheFile = false;
        private bool cacheOnly = true;


        public NoteLister(NotesContext context)
        {
            this.context = context;

            InitializeComponent();
            this.Icon = Resources.Icon1;

            LoadCachedNotes();
        }

        private void LoadCachedNotes()
        {
            listNotes.Columns.Clear();
            listNotes.Items.Clear();

            string cahchePath = XmlManager.getChachePath();


            //Fill with data
            ListViewItem newItem;
            //DateTime weekAgo = DateTime.Now.AddDays(-7);
            if (!showingTrash)
            {
                //Establish columns
                listNotes.Columns.Add("DateColumn", "Date created", 120);
                listNotes.Columns.Add("TextColumn", "Note", 400);

                if (XmlManager.CachedNotes.Count == 0)
                {
                    XmlManager.WriteCache(cahchePath, context);
                    XmlManager.CachedNotes = XmlManager.ReadChache(cahchePath);
                }

                foreach (var note in XmlManager.CachedNotes)
                {
                    newItem = new ListViewItem(new String[] { "", "" });
                    newItem.Tag = note.ID;
                    newItem.SubItems[0].Name = listNotes.Columns["DateColumn"].Name;
                    newItem.SubItems[0].Text = note.noteCreated.ToString();
                    newItem.SubItems[1].Name = listNotes.Columns["TextColumn"].Name;
                    newItem.SubItems[1].Text = note.noteText;

                    listNotes.Items.Add(newItem);
                }

            }
            listNotes.Sort();
            this.listNotes.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);
            listNotes.Refresh();
            Refresh();
        }

        //[MethodImpl(MethodImplOptions.Synchronized)]
        private void ListHot()
        {
            Cursor = Cursors.WaitCursor;
            listNotes.Columns.Clear();
            listNotes.Items.Clear();

            //Fill with data
            ListViewItem newItem;
            //DateTime weekAgo = DateTime.Now.AddDays(-7);
            if (!showingTrash)
            {
                //Establish columns
                listNotes.Columns.Add("DateColumn", "Date created", 120);
                listNotes.Columns.Add("TextColumn", "Note", 400);
                foreach (var note in context.Notes) //(Not any more. Super slow down .Where(row => row.noteCreated > weekAgo) )note created in last 7 days
                {
                    newItem = new ListViewItem(new String[] { "", "" });
                    newItem.Tag = note.ID;
                    newItem.SubItems[0].Name = listNotes.Columns["DateColumn"].Name;
                    newItem.SubItems[0].Text = note.noteCreated.ToString();
                    newItem.SubItems[1].Name = listNotes.Columns["TextColumn"].Name;
                    newItem.SubItems[1].Text = note.noteText;

                    listNotes.Items.Add(newItem);
                }
            }
            else
            {

                //Establish columns
                listNotes.Columns.Add("DelColumn", "Date deleted", 120);
                listNotes.Columns.Add("DateColumn", "Date created", 120);
                listNotes.Columns.Add("TextColumn", "Note", 400);
                foreach (var note in context.Deleted) //(Not any more. Super slow down .Where(row => row.noteCreated > weekAgo) )note created in last 7 days
                {
                    newItem = new ListViewItem(new String[] { "", "", "" });
                    newItem.Tag = note.ID;
                    newItem.SubItems[0].Name = listNotes.Columns["DelColumn"].Name;
                    newItem.SubItems[0].Text = note.noteDeleted.ToString();
                    newItem.SubItems[1].Name = listNotes.Columns["DateColumn"].Name;
                    newItem.SubItems[1].Text = note.noteCreated.ToString();
                    newItem.SubItems[2].Name = listNotes.Columns["TextColumn"].Name;
                    newItem.SubItems[2].Text = note.noteText;

                    listNotes.Items.Add(newItem);
                }
            }

            listNotes.Sort();
            // Set the ListViewItemSorter property to a new ListViewItemComparer  
            // object.  
            this.listNotes.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);

            //listNotes.Sorting = SortOrder.Descending;
            listNotes.Refresh();
            Refresh();
            Cursor = Cursors.Default;
        }

        private void listNotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listNotes.SelectedItems.Count == 0) return;
            selectedNoteID = listNotes.SelectedItems[0].Tag;
        }

        private void listNotes_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (selectedNoteID == null) return;

            int currID = (int)selectedNoteID;
            //NoteDetail detail = new NoteDetail(context.Notes.Single(note => note.ID == currID));
            NoteDetail detail = new NoteDetail(XmlManager.CachedNotes.Single(note => note.ID == currID));
            detail.ShowDialog();
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            //if (selectedNoteID == null) return;

            //int currID = (int)selectedNoteID;

            foreach (ListViewItem item in listNotes.SelectedItems)
            {
                int currID = (int)item.Tag;

                context.RemoveNoteByID(currID);
            }
            MessyNote.recreateCache();
            ListHot();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NoteLister_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btn_export_Click(object sender, EventArgs e)
        {
            SaveFileDialog folderBrowserDialog1 = new SaveFileDialog();
            folderBrowserDialog1.FileName = "D:\\Notes.xml";
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Export(folderName, context);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Import(folderName, context, true);
            }
        }

        private void btn_trash_Click(object sender, EventArgs e)
        {
            showingTrash = !showingTrash;
            if (showingTrash)
            {
                btn_trash.Text = "Hide trash";

                ListHot();
            }
            else
            {
                btn_trash.Text = "Show trash";

                ListHot();
            }
        }

        //https://social.msdn.microsoft.com/Forums/vstudio/en-US/499ce6d4-b20c-4675-89f6-08801d404fd0/sort-listview-items-by-date?forum=csharpgeneral
        class ListViewItemComparer : IComparer
        {
            private int col;
            private SortOrder order;
            public ListViewItemComparer(int column, SortOrder order)
            {
                col = column;
                this.order = order;
            }

            public int Compare(object x, object y)
            {
                int returnVal;
                // Determine whether the type being compared is a date type.  
                try
                {
                    // Parse the two objects passed as a parameter as a DateTime.  
                    System.DateTime firstDate =
                            DateTime.Parse(((ListViewItem)x).SubItems[col].Text);
                    System.DateTime secondDate =
                            DateTime.Parse(((ListViewItem)y).SubItems[col].Text);
                    // Compare the two dates.  
                    returnVal = DateTime.Compare(firstDate, secondDate);
                }
                // If neither compared object has a valid date format, compare  
                // as a string.  
                catch
                {
                    // Compare the two items as a string.  
                    returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                ((ListViewItem)y).SubItems[col].Text);
                }
                // Determine whether the sort order is descending.  
                if (order == SortOrder.Descending)
                    // Invert the value returned by String.Compare.  
                    returnVal *= -1;
                return returnVal;
            }
        }

        private void btn_loadDB_Click(object sender, EventArgs e)
        {
            cacheOnly = false;
            btn_trash.Enabled = true;
            ListHot();
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            List<Note> notes = new List<Note>();
            if (cacheOnly)
            {
                foreach (Note note in XmlManager.CachedNotes)
                {
                    if (note.noteText.Contains(tb_search.Text))
                        notes.Add(note);
                }
            }
            else
            {
                if (!showingTrash)
                {
                    foreach (Note note in context.Notes)
                        if (note.noteText.Contains(tb_search.Text))
                            notes.Add(note);
                }
                else
                {
                    foreach (DeletedNote note in context.Deleted)
                        if (note.noteText.Contains(tb_search.Text))
                            notes.Add(new Note() { noteCreated = note.noteCreated, noteText = note.noteText, ID = note.ID});
                }
            }
            ListNotes(notes);
        }

        private void ListNotes(List<Note> notes)
        {
            listNotes.Columns.Clear();
            listNotes.Items.Clear();

            string cahchePath = XmlManager.getChachePath();

            //Fill with data
            ListViewItem newItem;
            //DateTime weekAgo = DateTime.Now.AddDays(-7);
            //Establish columns
            listNotes.Columns.Add("DateColumn", "Date created", 120);
            listNotes.Columns.Add("TextColumn", "Note", 400);



            foreach (var note in notes)
            {
                newItem = new ListViewItem(new String[] { "", "" });
                newItem.Tag = note.ID;
                newItem.SubItems[0].Name = listNotes.Columns["DateColumn"].Name;
                newItem.SubItems[0].Text = note.noteCreated.ToString();
                newItem.SubItems[1].Name = listNotes.Columns["TextColumn"].Name;
                newItem.SubItems[1].Text = note.noteText;

                listNotes.Items.Add(newItem);
            }
            listNotes.Sort();
            this.listNotes.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);
            listNotes.Refresh();
            Refresh();
        }

       
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //import merge
            OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Import(folderName, context, true);
            }
        }

        private void overrideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //import override
            OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Import(folderName, context, false);
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog folderBrowserDialog1 = new SaveFileDialog();
            folderBrowserDialog1.FileName = "D:\\Notes.xml";
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Export(folderName, context);
            }
        }


        private void removeDuplicitiesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Cursor = Cursors.WaitCursor;


            foreach (Note note in context.Notes)
            {
                DbEntityEntry dbEntityEntry = context.Entry(note);
                if (dbEntityEntry.State == EntityState.Deleted)
                {
                    continue;
                }

                Note[] tmp = context.Notes.Where(nt => nt.ID != note.ID && nt.noteText.Equals(note.noteText)).ToArray();
                for (int i = 0; i < tmp.Length; i++)
                    context.Notes.Remove(tmp[i]);
            }
            context.SaveChanges();

            foreach (DeletedNote note in context.Deleted)
            {
                DbEntityEntry dbEntityEntry = context.Entry(note);
                if (dbEntityEntry.State == EntityState.Deleted)
                {
                    continue;
                }

                DeletedNote[] tmp = context.Deleted.Where(nt => nt.ID != note.ID && nt.noteText.Equals(note.noteText)).ToArray();
                for (int i = 0; i < tmp.Length; i++)
                    context.Deleted.Remove(tmp[i]);
            }
            context.SaveChanges();
            
            MessyNote.recreateCache();
            ListHot();
            Cursor = Cursors.Default;
        }

        private void invalidateCacheToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessyNote.recreateCache();
            LoadCachedNotes();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}

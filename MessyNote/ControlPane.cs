﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote
{
    /// <summary>
    /// Settings panel for MessyNotes key manager 
    /// </summary>
    public partial class ControlPanel : UserControl
    {
        MessyNote notes;
        public ControlPanel(MessyNote notes)
        {
            InitializeComponent();
            this.notes = notes;
            chk_noteWriter.Checked = notes.getHelpers()[0].isEnabled();
            chk_noteReade.Checked = notes.getHelpers()[1].isEnabled();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            notes.getHelpers()[0].setEnabled(chk_noteWriter.Checked);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            notes.getHelpers()[1].setEnabled(chk_noteReade.Checked);
        }

        private void btn_clearCache_Click(object sender, EventArgs e)
        {
            MessyNote.recreateCache();
        }
    }
}

﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote
{
    /// <summary>
    /// Helper class opening form with list of notes
    /// </summary>
    class NoteReader : Helper, IHelping
    {
        NotesContext context;
        NoteLister noteDialog;
        Thread dialogThread;

        public NoteReader(NotesContext context)
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.Alt | Keys.N) };
            this.context = context;
            noteDialog = null;
        }

        bool enabled = true;
        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        public bool processKeyStroke(List<KeyCombo> recentlyPressedKeys)
        {
            if (!enabled) return false;
            Logger.LogInfo($"{nameof(NoteReader)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(NoteReader)} Combination doesn't match.");
                return false;
            }


            if (noteDialog != null)
            {
                Logger.LogWarn($"{nameof(NoteReader)} already opened." );
                return true;
            }
            try
            {
                dialogThread = new Thread(new ThreadStart(showNoteLister));
                dialogThread.SetApartmentState(ApartmentState.STA);
                dialogThread.Start();
                dialogThread = null;
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(NoteReader)} " + e.StackTrace);
                return false;
            }

            return true;
        }

        private void showNoteLister()
        {
            noteDialog = new NoteLister(context);
            noteDialog.ShowDialog();
            noteDialog = null;
        }

        public bool suppressHey()
        {
            return true;
        }
    }
}

﻿using Amenities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MessyNote
{
    class XmlManager
    {

        public static string chache_folder = "MESSY_NOTES_CACHE_FOLDER";
        public static string default_chache_folder = "";
        public static string defaultCacheFileName = "LillMessyNotesCache.xml";

        private static int NUMBER_OF_CACHED_NOTES = 100;

        private static List<Note> cachedNotes = null;
        public static List<Note> CachedNotes
        {
            get
            {
                if (cachedNotes == null)
                    cachedNotes = ReadChache(getChachePath());
                return cachedNotes;
            }
            set { cachedNotes = value; }
        }

        public static bool CacheModified { get; internal set; }

        public static void WriteCache(String path, NotesContext data)
        {
            CacheModified = false;
            using (XmlWriter writer = XmlWriter.Create(path, new XmlWriterSettings() { Indent = true }))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("MessyNotes");

                writer.WriteStartElement("Notes");

                //DateTime weekAgo = DateTime.Now.AddDays(-7);
                foreach (var note in data.Notes.OrderByDescending(note => note.noteCreated).Take(NUMBER_OF_CACHED_NOTES))//Where(note => note.noteCreated > weekAgo))
                {

                    writer.WriteStartElement("Note");
                    writer.WriteElementString(nameof(note.ID), note.ID.ToString());
                    writer.WriteElementString(nameof(note.noteCreated), note.noteCreated.ToString());
                    writer.WriteElementString(nameof(note.noteText), note.noteText);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Close();
            }
        }

        internal static string getChachePath()
        {
            string path = Settings.getSettingValue(XmlManager.chache_folder, XmlManager.default_chache_folder);
            if (string.IsNullOrEmpty(path))
                path = Settings.getInstallationPath() + defaultCacheFileName; ;
            return path;
        }

        public static List<Note> ReadChache()
        {
            return ReadChache(XmlManager.getChachePath());
        }

        public static List<Note> ReadChache(String path)
        {
            List<Note> notes = new List<Note>();

            if(File.Exists(path))
            try
            {
                using (XmlReader reader = XmlReader.Create(path))
                {
                    while (reader.ReadToFollowing("Note"))
                    {
                        reader.Read();
                        Note note = new Note();
                        while (reader.IsStartElement())
                        {
                            if (reader.IsEmptyElement)
                                reader.Read();
                            else
                                switch (reader.Name)
                                {
                                    case nameof(note.ID): reader.ReadStartElement(); note.ID = reader.ReadContentAsInt(); reader.ReadEndElement(); break;
                                    case nameof(note.noteText): reader.ReadStartElement(); note.noteText = reader.ReadContentAsString(); reader.ReadEndElement(); break;
                                    case nameof(note.noteCreated): reader.ReadStartElement(); note.noteCreated = DateTime.Parse(reader.ReadContentAsString()); reader.ReadEndElement(); break;
                                    default: reader.Skip(); break;
                                }
                        }
                        notes.Add(note);
                    }
                        reader.Close();
                }
            }
            catch (Exception e)
            {
                Logger.LogWarn("Cache file not found");
            }
            cachedNotes = notes;
            return notes;
        }

        /// <summary>
        /// Ulozeni s vyuzitim XmlWriteru.
        /// </summary>
        /// <param name="data">Ukladana data</param>
        public static void Export(String path, NotesContext data)
        {
            using (XmlWriter writer = XmlWriter.Create(path, new XmlWriterSettings() { Indent = true }))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("MessyNotes");

                writer.WriteStartElement("Notes");
                foreach (var note in data.Notes)
                {

                    writer.WriteStartElement("Note");
                    writer.WriteElementString(nameof(note.ID), note.ID.ToString());
                    writer.WriteElementString(nameof(note.noteCreated), note.noteCreated.ToString());
                    writer.WriteElementString(nameof(note.noteText), note.noteText);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                writer.WriteStartElement("DeletedNotes");
                foreach (var note in data.Deleted)
                {
                    writer.WriteStartElement("DeletedNote");
                    writer.WriteElementString(nameof(note.ID), note.ID.ToString());
                    writer.WriteElementString(nameof(note.noteCreated), note.noteCreated.ToString());
                    writer.WriteElementString(nameof(note.noteText), note.noteText);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Close();
            }
        }

        /// <summary>
        /// Nacteni s vyuzitim XmlReaderu.
        /// </summary>
        /// <returns>Nactena data</returns>
        public static void Import(String path, NotesContext data, bool merge)
        {
            List<Note> notes = new List<Note>();
            List<DeletedNote> deleted = new List<DeletedNote>();
            try
            {
                using (XmlReader reader = XmlReader.Create(path))
                {
                    while (reader.ReadToFollowing("Note"))
                    {
                        reader.Read();
                        Note note = new Note();
                        while (reader.IsStartElement())
                        {
                            if (reader.IsEmptyElement)
                                reader.Read();
                            else
                                switch (reader.Name)
                                {
                                    case nameof(note.ID): reader.ReadStartElement(); note.ID = reader.ReadContentAsInt(); reader.ReadEndElement(); break;
                                    case nameof(note.noteText): reader.ReadStartElement(); note.noteText = reader.ReadContentAsString().Replace("\n", "\r\n"); reader.ReadEndElement(); break;
                                    case nameof(note.noteCreated): reader.ReadStartElement(); note.noteCreated = DateTime.Parse( reader.ReadContentAsString()); reader.ReadEndElement(); break;
                                    default: reader.Skip(); break;
                                }
                        }
                        notes.Add(note);
                    }
                    reader.Close();
                }
                using (XmlReader reader = XmlReader.Create(path))
                {
                    while (reader.ReadToFollowing("DeletedNote"))
                    {
                        reader.Read();
                        DeletedNote note = new DeletedNote();
                        while (reader.IsStartElement())
                        {
                            if (reader.IsEmptyElement)
                                reader.Read();
                            else
                                switch (reader.Name)
                                {
                                    case nameof(note.ID): reader.ReadStartElement(); note.ID = reader.ReadContentAsInt(); reader.ReadEndElement(); break;
                                    case nameof(note.noteText): reader.ReadStartElement(); note.noteText = reader.ReadContentAsString().Replace("\n", "\r\n"); reader.ReadEndElement(); break;
                                    case nameof(note.noteCreated): reader.ReadStartElement(); note.noteCreated = DateTime.Parse( reader.ReadContentAsString()); reader.ReadEndElement(); break;
                                    case nameof(note.noteDeleted): reader.ReadStartElement(); note.noteDeleted = DateTime.Parse(reader.ReadContentAsString()); reader.ReadEndElement(); break;
                                    default: reader.Skip(); break;
                                }
                        }
                        if (note.noteDeleted == DateTime.MinValue)
                            note.noteDeleted = DateTime.Now.AddDays(-5);
                        deleted.Add(note);
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Failed to import Notes", "Error Importing",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            if (!merge)
            {
                data.Notes.RemoveRange(data.Notes.Where(x => x.ID >= 0));
                data.Deleted.RemoveRange(data.Deleted.Where(x => x.ID >= 0));
                data.SaveChanges();
                data.Database.ExecuteSqlCommand("DBCC CHECKIDENT (Notes, RESEED, 0)");
                data.Database.ExecuteSqlCommand("DBCC CHECKIDENT (DeletedNotes, RESEED, 0)");

                data.Notes.AddRange(notes);
                data.SaveChanges();
                data.Deleted.AddRange(deleted);
                data.SaveChanges();
            }
            else
            {
                foreach(Note note in notes)
                {
                    if(!data.Notes.Any(nt => nt.ID == note.ID))
                    {
                        data.Notes.Add(note);
                        continue;
                    }

                    if (!data.Notes.First(nt => nt.ID == note.ID).noteText.Equals(note.noteText))
                        data.AddNote(note.noteText, note.noteCreated);
                }

                foreach (DeletedNote note in deleted)
                {
                    if (!data.Deleted.Any(nt => nt.ID == note.ID))
                    {
                        data.Deleted.Add(note);
                        continue;
                    }

                    if (!data.Deleted.First(nt => nt.ID == note.ID).noteText.Equals(note.noteText))
                        data.AddDeletedNote(note.noteText, note.noteCreated, note.noteDeleted);
                }
                data.SaveChanges();
            }
        }
    }
}


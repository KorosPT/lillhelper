﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Threading.Tasks;
using Amenities;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace MessyNote
{
    /// <summary>
    /// Helper writing into DB
    /// </summary>
    class NoteWriter : Helper, IHelping
    {
        private const int TIME_PERIOD_SECONDS = 2;

        private NotesContext context;

        public NoteWriter(NotesContext context)
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.C), new KeyCombo(Keys.Control | Keys.C) };
            this.context = context;
        }

        bool enabled = true;
        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
        }
        
        public bool processKeyStroke(List<KeyCombo> recentlyPressedKeys)
        {
            if (!enabled) return false;
            Logger.LogInfo($"{nameof(NoteWriter)} called.");

            if(!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(NoteWriter)} Combination doesn't match.");
                return false;
            }

            //Get text in clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }

                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            try
            {
                //context.Notes.Add(new Note() { noteText = clipText, noteCreated = DateTime.Now });
                //context.SaveChanges();
                context.AddNoteAsync(clipText);
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(NoteWriter)} " + e.StackTrace);
                return false;
            }
            return true;
        }

        public bool suppressHey()
        {
            return false;
        }

        public override void Dispose()
        {
            saveNewCache();
        }

        public void saveNewCache()
        {
            if (XmlManager.CacheModified)
                XmlManager.WriteCache(XmlManager.getChachePath(), context);
        }
    }
}

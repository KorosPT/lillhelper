﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessyNote
{
    /// <summary>
    /// Notes DB context
    /// </summary>
    class NotesContext : DbContext
    {
        public DbSet<Note> Notes { get; set; }
        public DbSet<DeletedNote> Deleted { get; set; }

        private Task ongoingActivity = null;

        internal void RemoveNoteByID(int currID)
        {
            if (ongoingActivity != null)
            {
                ongoingActivity.Wait();
                ongoingActivity = null;
            }

            Note deleteThis = GetNoteByID(currID);

            Deleted.Add(new DeletedNote() { noteCreated = deleteThis.noteCreated, noteText = deleteThis.noteText, noteDeleted = DateTime.Now });

            //Notes.Attach(deleteThis);
            Notes.Remove(Notes.First(note => note.ID == currID));

            XmlManager.CachedNotes.Remove(deleteThis);
            XmlManager.CacheModified = true;

            ongoingActivity = SaveChangesAsync();
        }

        internal void AddNoteAsync(string clipText)
        {
            AddNoteAsync(clipText, DateTime.Now);
        }

        internal void AddNoteAsync(string clipText, DateTime date)
        {
            if (ongoingActivity != null)
            {
                ongoingActivity.Wait();
                ongoingActivity = null;
            }

            Note newNote = new Note() { noteText = clipText, noteCreated = date };
            Notes.Add(newNote);
            XmlManager.CachedNotes.Add(newNote);
            XmlManager.CacheModified = true;
            ongoingActivity = SaveChangesAsync();
        }

        internal void WaitForMe()
        {
            if (ongoingActivity != null)
            {
                ongoingActivity.Wait();
                ongoingActivity = null;
            }
        }

        internal void AddNote(string clipText, DateTime date)
        {
            Note newNote = new Note() { noteText = clipText, noteCreated = date };
            Notes.Add(newNote);
            SaveChanges();
        }
        internal void AddDeletedNote(string clipText, DateTime dateCreated, DateTime dateDeleted)
        {
            DeletedNote newNote = new DeletedNote() { noteText = clipText, noteCreated = dateCreated, noteDeleted = dateDeleted };
            Deleted.Add(newNote);
            SaveChanges();
        }

        internal Note GetNoteByID(int id)
        {
            return Notes.First(note => note.ID == id);
        }
    }
}

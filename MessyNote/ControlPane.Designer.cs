﻿namespace MessyNote
{
    partial class ControlPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.chk_noteWriter = new System.Windows.Forms.CheckBox();
            this.chk_noteReade = new System.Windows.Forms.CheckBox();
            this.btn_clearCache = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Location = new System.Drawing.Point(13, 9);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(140, 17);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "MessyNotes Settings";
            // 
            // chk_noteWriter
            // 
            this.chk_noteWriter.AutoSize = true;
            this.chk_noteWriter.Location = new System.Drawing.Point(26, 35);
            this.chk_noteWriter.Name = "chk_noteWriter";
            this.chk_noteWriter.Size = new System.Drawing.Size(98, 21);
            this.chk_noteWriter.TabIndex = 1;
            this.chk_noteWriter.Text = "NoteWriter";
            this.chk_noteWriter.UseVisualStyleBackColor = true;
            this.chk_noteWriter.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chk_noteReade
            // 
            this.chk_noteReade.AutoSize = true;
            this.chk_noteReade.Location = new System.Drawing.Point(26, 62);
            this.chk_noteReade.Name = "chk_noteReade";
            this.chk_noteReade.Size = new System.Drawing.Size(107, 21);
            this.chk_noteReade.TabIndex = 2;
            this.chk_noteReade.Text = "NoteReader";
            this.chk_noteReade.UseVisualStyleBackColor = true;
            this.chk_noteReade.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // btn_clearCache
            // 
            this.btn_clearCache.Location = new System.Drawing.Point(16, 89);
            this.btn_clearCache.Name = "btn_clearCache";
            this.btn_clearCache.Size = new System.Drawing.Size(127, 29);
            this.btn_clearCache.TabIndex = 3;
            this.btn_clearCache.Text = "Invalidate cache";
            this.btn_clearCache.UseVisualStyleBackColor = true;
            this.btn_clearCache.Click += new System.EventHandler(this.btn_clearCache_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_clearCache);
            this.Controls.Add(this.chk_noteReade);
            this.Controls.Add(this.chk_noteWriter);
            this.Controls.Add(this.lbl_title);
            this.Name = "ControlPanel";
            this.Size = new System.Drawing.Size(312, 164);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.CheckBox chk_noteWriter;
        private System.Windows.Forms.CheckBox chk_noteReade;
        private System.Windows.Forms.Button btn_clearCache;
    }
}

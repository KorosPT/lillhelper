﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessyNote
{
    /// <summary>
    /// Simple note that acan be saved in DB, but isn't. 
    /// Here for inheritance purposes
    /// </summary>
    class TheOverNote
    {
        public int ID { get; set; }
        public String noteText { get; set; }
        public DateTime noteCreated { get; set; }

    }

    /// <summary>
    /// Note saved in the DB
    /// </summary>
    class Note : TheOverNote
    {
    }

    /// <summary>
    /// Deleted note saved in the DB
    /// </summary>
    class DeletedNote : TheOverNote
    {       
        public DateTime noteDeleted { get; set; }
    }
}

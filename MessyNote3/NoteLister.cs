﻿using MessyNote3.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote3
{
    public partial class NoteLister : Form
    {
        private NotesContext context;
        object selectedNoteID = null;
        NoteBundle currBundle = null;
        private bool filtering = false;
        private string filter = "";
        public NoteLister(NotesContext context)
        {
            InitializeComponent();
            this.context = context;
            this.Icon = Resources.Icon1;

            currBundle = context.getNoteBundle();
            LoadData();
        }

        private void LoadData()
        {
            if (currBundle == null)
                return;

            lbl_num.Text = currBundle.ID.ToString();

            //Establish columns
            listNotes.Items.Clear();
            listNotes.Columns.Clear();
            listNotes.Columns.Add("DateColumn", "Date created", 140);
            listNotes.Columns.Add("TextColumn", "Note", 400);

            if (filtering)
            {
                foreach (NoteBundle bnd in context.Bundles)
                    loadBundle(bnd);
            }
            loadBundle(currBundle);


            listNotes.Sort();
            this.listNotes.ListViewItemSorter = new ListViewItemComparer(0, SortOrder.Descending);
            listNotes.Refresh();
            Refresh();
        }

        private void loadBundle(NoteBundle bundle)
        {
            ListViewItem newItem;
            foreach (Note note in bundle.Notes)
            {
                if (filtering)
                {
                    if (!note.noteText.Contains(filter))
                        continue;
                }

                newItem = new ListViewItem(new String[] { "", "" });
                newItem.Tag = note.ID;
                newItem.SubItems[0].Name = listNotes.Columns["DateColumn"].Name;
                newItem.SubItems[0].Text = note.noteCreated.ToString();
                newItem.SubItems[1].Name = listNotes.Columns["TextColumn"].Name;
                newItem.SubItems[1].Text = note.noteText;

                listNotes.Items.Add(newItem);
            }
        }

        private void NoteLister_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }


        private void listNotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listNotes.SelectedItems.Count == 0) return;
            selectedNoteID = listNotes.SelectedItems[0].Tag;
        }

        private void listNotes_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (selectedNoteID == null) return;

            int currID = (int)selectedNoteID;
            //NoteDetail detail = new NoteDetail(context.Notes.Single(note => note.ID == currID));
            NoteDetail detail = new NoteDetail(context.getNote(currID));
            detail.ShowDialog();
        }


        private void btn_back_Click(object sender, EventArgs e)
        {
            currBundle = context.getNoteBundle(currBundle.ID - 1);
            if (currBundle == null)
                currBundle = context.getNoteBundle(0);
            LoadData();
        }

        private void btn_backAll_Click(object sender, EventArgs e)
        {
            currBundle = context.getNoteBundle(0);
            LoadData();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            currBundle = context.getNoteBundle(currBundle.ID + 1);
            if (currBundle == null)
                currBundle = context.getNoteBundle(context.CountBundles() - 1);
            LoadData();
        }

        private void btn_nextAll_Click(object sender, EventArgs e)
        {
            currBundle = context.getNoteBundle(context.CountBundles() - 1);
            LoadData();
        }




        //https://social.msdn.microsoft.com/Forums/vstudio/en-US/499ce6d4-b20c-4675-89f6-08801d404fd0/sort-listview-items-by-date?forum=csharpgeneral
        class ListViewItemComparer : IComparer
        {
            private int col;
            private SortOrder order;
            public ListViewItemComparer(int column, SortOrder order)
            {
                col = column;
                this.order = order;
            }

            public int Compare(object x, object y)
            {
                int returnVal;
                // Determine whether the type being compared is a date type.  
                try
                {
                    // Parse the two objects passed as a parameter as a DateTime.  
                    System.DateTime firstDate =
                            DateTime.Parse(((ListViewItem)x).SubItems[col].Text);
                    System.DateTime secondDate =
                            DateTime.Parse(((ListViewItem)y).SubItems[col].Text);
                    // Compare the two dates.  
                    returnVal = DateTime.Compare(firstDate, secondDate);
                }
                // If neither compared object has a valid date format, compare  
                // as a string.  
                catch
                {
                    // Compare the two items as a string.  
                    returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text,
                                ((ListViewItem)y).SubItems[col].Text);
                }
                // Determine whether the sort order is descending.  
                if (order == SortOrder.Descending)
                    // Invert the value returned by String.Compare.  
                    returnVal *= -1;
                return returnVal;
            }
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_del_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listNotes.SelectedItems)
            {
                int currID = (int)item.Tag;

                currBundle.RemoveNote(currID);
            }
            LoadData();
        }

        private void tb_search_TextChanged(object sender, EventArgs e)
        {
            filtering = !string.IsNullOrWhiteSpace(tb_search.Text);
            filter = tb_search.Text;
            LoadData();
        }

        private void mergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //import merge
            OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Import(folderName, context, true);
            }
            currBundle = context.getNoteBundle();
            LoadData();
        }

        private void overrideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //import override
            OpenFileDialog folderBrowserDialog1 = new OpenFileDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Import(folderName, context, false);
            }
            currBundle = context.getNoteBundle();
            LoadData();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog folderBrowserDialog1 = new SaveFileDialog();
            folderBrowserDialog1.FileName = "D:\\Notes.xml";
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string folderName = folderBrowserDialog1.FileName;
                XmlManager.Export(folderName, context);
            }
        }

        private void NoteLister_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
    }
}

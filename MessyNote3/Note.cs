﻿using Amenities;
using System;

namespace MessyNote3
{
    public class Note
    {
            public int ID { get; set; }
            public String noteText { get; set; }
            public DateTime noteCreated { get; set; }

        private string separator1 = Convert.ToChar(0x00).ToString();
        private string separator2 = Convert.ToChar(0x00).ToString();

        public override string ToString()
        {
            return $"{ID}{separator1}{noteCreated.ToString()}{separator2}{noteText}";
        }

        public void FromString(string text)
        {

            string textOrig = text;
            try
            {
                string idStr = text.Substring(0, text.IndexOf(separator1));
                text = text.Substring(text.IndexOf(separator1));
                idStr = idStr.Replace(separator2, "");

                string dateStr = text.Substring(1, text.LastIndexOf(separator2) );
                text = text.Substring(text.IndexOf(separator1));
                dateStr = dateStr.Replace(separator2, "");

                ID = Int32.Parse(idStr);
                noteCreated = DateTime.Parse(dateStr);
                noteText = text.Substring(text.LastIndexOf(separator2) + 1).Replace(separator2, "");
            }catch(Exception e)
            {
                Logger.LogError($"{nameof(Note)}failed parsing note. " + text);
            }
        }
    }
}
﻿namespace MessyNote3
{
    partial class NoteDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_date = new System.Windows.Forms.Label();
            this.txt_noteText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(10, 11);
            this.lbl_date.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(35, 13);
            this.lbl_date.TabIndex = 0;
            this.lbl_date.Text = "label1";
            // 
            // txt_noteText
            // 
            this.txt_noteText.Location = new System.Drawing.Point(12, 37);
            this.txt_noteText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_noteText.Multiline = true;
            this.txt_noteText.Name = "txt_noteText";
            this.txt_noteText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_noteText.Size = new System.Drawing.Size(236, 123);
            this.txt_noteText.TabIndex = 1;
            this.txt_noteText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            // 
            // NoteDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 185);
            this.Controls.Add(this.txt_noteText);
            this.Controls.Add(this.lbl_date);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NoteDetail";
            this.Text = "NoteDetail";
            this.Shown += new System.EventHandler(this.NoteDetail_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NoteDetail_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.TextBox txt_noteText;
    }
}
﻿using Amenities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MessyNote3
{
    class XmlManager
    {
       
        /// <summary>
        /// Ulozeni s vyuzitim XmlWriteru.
        /// </summary>
        /// <param name="data">Ukladana data</param>
        public static void Export(String path, NotesContext data)
        {
            using (XmlWriter writer = XmlWriter.Create(path, new XmlWriterSettings() { Indent = true }))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("MessyNotes");

                writer.WriteStartElement("Notes");
                foreach (NoteBundle bnd in data.Bundles)
                     foreach (Note note in bnd.Notes)
                    {

                    writer.WriteStartElement("Note");
                    writer.WriteElementString(nameof(note.ID), note.ID.ToString());
                    writer.WriteElementString(nameof(note.noteCreated), note.noteCreated.ToString());
                    writer.WriteElementString(nameof(note.noteText), note.noteText);
                    writer.WriteEndElement();
                    }
                writer.WriteEndElement();

                
                writer.WriteEndElement();
                writer.Close();
            }
        }

        /// <summary>
        /// Nacteni s vyuzitim XmlReaderu.
        /// </summary>
        /// <returns>Nactena data</returns>
        public static void Import(String path, NotesContext data, bool merge)
        {
            List<Note> notes = new List<Note>();
            try
            {
                using (XmlReader reader = XmlReader.Create(path))
                {
                    while (reader.ReadToFollowing("Note"))
                    {
                        reader.Read();
                        Note note = new Note();
                        while (reader.IsStartElement())
                        {
                            if (reader.IsEmptyElement)
                                reader.Read();
                            else
                                switch (reader.Name)
                                {
                                    case nameof(note.ID): reader.ReadStartElement(); note.ID = reader.ReadContentAsInt(); reader.ReadEndElement(); break;
                                    case nameof(note.noteText): reader.ReadStartElement(); note.noteText = reader.ReadContentAsString().Replace("\n", "\r\n"); reader.ReadEndElement(); break;
                                    case nameof(note.noteCreated): reader.ReadStartElement(); note.noteCreated = DateTime.Parse( reader.ReadContentAsString()); reader.ReadEndElement(); break;
                                    default: reader.Skip(); break;
                                }
                        }
                        notes.Add(note);
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Failed to import Notes", "Error Importing",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            if (!merge)
            {
                //overrite
                data.wipeData();
                foreach (Note note in notes)
                    data.AddNote(note.noteText,note.noteCreated);
            }
            else
            {
                //merge
                foreach(Note note in notes)
                {
                    if(data.getNote(note.ID) == null)
                    {
                        data.AddNote(note.noteText, note.noteCreated);
                        continue;
                    }

                    if (!data.getNote(note.ID).noteText.Equals(note.noteText))
                        data.AddNote(note.noteText, note.noteCreated);
                }
            }
        }
    }
}


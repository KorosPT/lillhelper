﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote3
{
    /// <summary>
    /// Key manager MassyNotes has two IHelping classes
    /// NoteWriter saves text in clipboard into database
    /// NoteReader shows list of notes in database
    /// </summary>
    public class MessyNote3 : IKeyManaging
    {
        private NoteWriter writer;
        private NoteReader reader;
        private NotesContext context;

        public MessyNote3()
        {
            context = new NotesContext();
            writer = new NoteWriter(context);
            reader = new NoteReader(context);
        }

        public IHelping[] getHelpers()
        {
            return new IHelping[] { writer, reader };
        }

        public String getBriefDescription()
        {
            return "MessyNotes2";
        }

        public UserControl getControlPanel()
        {
            return new ControlPanel(this);
        }

        public override string ToString()
        {
            return getBriefDescription();
        }

        public MenuItem[] getMenuButtons()
        {
            return new MenuItem[0];
        }        
    }
}

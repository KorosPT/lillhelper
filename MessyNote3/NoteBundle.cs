﻿using Amenities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessyNote3
{
    public class NoteBundle
    {
        public static int BUNDLE_MAX_SIZE = 50;

        public string path { get; private set; }
        public int ID { get; private set; }
        public List<Note> Notes { get; private set; }
        public int CurrMaxID { get; private set; } = 0;


        private bool changed = false;

        public NoteBundle(string path, int ID)
        {
            this.path = path;
            this.ID = ID;
            this.Notes = new List<Note>();
            loadBundle();
        }

        public Note getNote(int ID)
        {
            foreach (Note nt in Notes)
            {
                if (ID == nt.ID)
                    return nt;
            }
            return null;
        }

        internal void NewNote(Note newNote)
        {
            changed = true;
            Notes.Add(newNote);
            if (newNote.ID > CurrMaxID)
                CurrMaxID = newNote.ID;
        }

        public int Count { get { return Notes.Count; } }

        private void loadBundle()
        {
            BinaryReader br;
            //reading from the file
            //try
            //{
            //    br = new BinaryReader(new FileStream(path, FileMode.Open));
            //}
            //catch (IOException e)
            //{
            //    Console.WriteLine(e.Message + "\n Cannot open file.");
            //    return;
            //}


            using (br = new BinaryReader(new FileStream(path, FileMode.OpenOrCreate)))
            {
                try
                {
                    if (br.BaseStream.Position == br.BaseStream.Length)
                    {
                        return;
                    }

                    string line = "";
                    ID = br.ReadInt32();
                    while (br.BaseStream.Position != br.BaseStream.Length && !string.IsNullOrEmpty(line = br.ReadString()))
                    {
                        Note tmpNote = new Note();
                        tmpNote.FromString(line);
                        Notes.Add(tmpNote);

                        if (tmpNote.ID > CurrMaxID)
                            CurrMaxID = tmpNote.ID;
                    }
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot read from file.");
                    return;
                }
            }
        }

        public void saveBundle()
        {
            if (!changed)
                return;

            string tmpPath = path;
            //if (!File.Exists(tmpPath))
            //{ 
            //    System.IO.File.Move(tmpPath, tmpPath+"~");
            //}

            BinaryWriter bw;
            ////create the file
            //try
            //{
            //    bw = new BinaryWriter(new FileStream(path, FileMode.Create));
            //}
            //catch (IOException e)
            //{
            //    Console.WriteLine(e.Message + "\n Cannot create file.");
            //    return;
            //}

            using (bw = new BinaryWriter(new FileStream(path, FileMode.Create)))
            {
                //writing into the file
                try
                {
                    bw.Write(ID);
                    foreach (Note note in Notes)
                        bw.Write(note.ToString());
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message + "\n Cannot write to file.");
                    return;
                }
            }
            changed = false;
        }

        internal void RemoveNote(int currID)
        {
            for(int i = 0; i < Notes.Count; i++)
            {
                if(Notes[i].ID == currID)
                {
                    Notes.RemoveAt(i);
                    changed = true;
                    return;
                }
            }
        }
    }
}

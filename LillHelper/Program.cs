﻿using Amenities;
using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LillHelper
{
    /// <summary>
    /// Starter class and logger init
    /// </summary>
    static class Program
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Amenities.Logger.Log = log;
                new KeyboardHook().Start();
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(LillHelper)}: {e.Message} {Environment.NewLine} {e.StackTrace}");
                MessageBox.Show(e.Message);
                throw e;
            }
        }
    }
}

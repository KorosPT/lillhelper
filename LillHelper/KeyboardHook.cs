﻿using Amenities;
using Common.Logging;
using CoreFunctionality;
using Gma.System.MouseKeyHook;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LillHelper
{
    /// <summary>
    /// Listens to the keyboard presses and sends them to CoreDispatcher
    /// </summary>
    class KeyboardHook
    {
        public static ILog Log { get; private set; }

        /// <summary>
        /// Thread containing CoreDispatcher
        /// </summary>
        public Thread coreThread;
        /// <summary>
        /// Main class processing key strokes 
        /// </summary>
        public CoreDispatcher core;
        /// <summary>
        /// Thread lies suspended over this until it is .Set
        /// Triggers processing of a key stroke
        /// </summary>
        AutoResetEvent coreAlarmclock;

        private IKeyboardMouseEvents m_Events;
        public void StartCore()
        {
            Log = Amenities.Logger.Log;
            
            Log.Info($"{nameof(KeyboardHook)} Core dispatcher starting.");

            coreAlarmclock = new AutoResetEvent(false);

            core = new CoreDispatcher(coreAlarmclock);
            core.Run();
        }

        /// <summary>
        /// Starts thread with core functionality
        /// </summary>
        /// <returns>All went well, application ended</returns>
        public bool Start()
        {
            Amenities.Logger.Log.Info($"{nameof(KeyboardHook)} Start command received.");

            //Starting the core service handling
            coreThread = new Thread(new ThreadStart(StartCore));
            coreThread.IsBackground = true;
            coreThread.Start();

            //Starting the keyboard hook
            SubscribeGlobal();

            //Start application - blocking
            Application.Run();

            Stop();

            return true;
        }

        /// <summary>
        /// Frees resources, unhooks keyboard hook
        /// </summary>
        /// <returns>Success?</returns>
        public bool Stop()
        {
            Log.Warn($"{nameof(KeyboardHook)} Stop command received.");

            //Stoping the Thread
            Settings.ShutingDown = true;
            coreAlarmclock.Set();
            coreAlarmclock.Close();
            coreThread.Join();

            //Stoping the hook
            Unsubscribe();

            return true;
        }

        /// <summary>
        /// Unsubscribes keyboard hook and resubscribes it
        /// </summary>
        private void SubscribeGlobal()
        {
            Unsubscribe();
            Subscribe(Hook.GlobalEvents());
        }

        /// <summary>
        /// Subscribes to keyboard hook
        /// </summary>
        /// <param name="events"></param>
        private void Subscribe(IKeyboardMouseEvents events)
        {
            m_Events = events;
            m_Events.KeyDown += OnKeyDown;
        }

        /// <summary>
        /// Unsubscribes keyboard hook listener
        /// </summary>
        private void Unsubscribe()
        {
            if (m_Events == null) return;
            m_Events.KeyDown -= OnKeyDown;

            m_Events.Dispose();
            m_Events = null;
        }

        /// <summary>
        /// When a key is pressed, send it to the core
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            //Wrap in wrapper class
            KeyCombo keyCombo = new KeyCombo(e);

            //Send to the core
            Tuple<bool, bool> result = CoreDispatcher.director.KeyPress(keyCombo);
            //Commented so I don't keylog all I do. Would hate to know all my passwords
            //Log.Info(string.Format("KeyDown \t {0}", keyCombo.ToString() + "\tProcessing:\t" + result.Item1.ToString()));

            //Should the key be consumed?
            if (result.Item1)
                e.SuppressKeyPress = true;

            //Should the coreDispatcher react to the key?
            if (result.Item2)
                coreAlarmclock.Set();
        }
    }
}

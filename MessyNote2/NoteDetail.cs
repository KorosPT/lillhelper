﻿using MessyNote2.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote2
{
    /// <summary>
    /// Detail of one note
    /// </summary>
    partial class NoteDetail : Form
    {
        public NoteDetail(Note note)
        {
            InitializeComponent();
            this.Icon = Resources.Icon1;

            showData(note);
        }

        private void showData(Note note)
        {
            lbl_date.Text = note.noteCreated.ToString();
            txt_noteText.Text = note.noteText;

        }

        private void NoteDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void NoteDetail_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
    }
}

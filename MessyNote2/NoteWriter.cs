﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote2
{
    class NoteWriter : Helper, IHelping
    {
        private const int TIME_PERIOD_SECONDS = 2;

        private NotesContext context;

        public NoteWriter(NotesContext context)
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.C), new KeyCombo(Keys.Control | Keys.C) };
            this.context = context;
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(NoteWriter)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(NoteWriter)} Combination doesn't match.");
                return false;
            }

            //Get text in clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }

                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            try
            {
                context.AddNote(clipText);
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(NoteWriter)} " + e.StackTrace);
                return false;
            }
            return true;
        }

        public override bool suppressKey(LinkedListNode<KeyCombo> pressedKey)
        {
            return false;
        }

        public override void Dispose()
        {
            context.living = false;
        }
    }
}

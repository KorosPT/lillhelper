﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote2
{
    public partial class ControlPanel : UserControl
    {
        private MessyNote2 messyNote2;

        public ControlPanel(MessyNote2 messyNote2)
        {
            this.messyNote2 = messyNote2;
            InitializeComponent();
        }
    }
}

﻿using Amenities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessyNote2
{
    public class NotesContext
    {
        public List<NoteBundle> Bundles { get; private set; }
        private const string filePath = "Data\\";
        private Thread saveThread;

        public bool living = true;

        public NotesContext()
        {
            Bundles = new List<NoteBundle>();
            loadBundles();

            saveThread = new Thread(new ThreadStart(periodicSaver));
            saveThread.SetApartmentState(ApartmentState.STA);
            saveThread.Start();
        }

        private void periodicSaver()
        {
            while (living)
            {
                Thread.Sleep(5000);
                foreach (NoteBundle bnd in Bundles)
                {
                    bnd.saveBundle();
                }
            }
        }

        public int CountBundles()
        {
            return Bundles.Count;
        }

        private void loadBundles()
        {
            string path = Settings.getInstallationPath()  + filePath;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("Notes*.dat"); 
            foreach (FileInfo file in Files)
            {
                if (file.Name.Contains("~"))
                    continue;

                try
                {
                    string idStr = file.Name.Replace(file.Extension,"").Substring(file.Name.IndexOf("s")+1);
                    Bundles.Add(new NoteBundle(file.FullName, Int32.Parse(idStr)));
                }catch
                {
                    Logger.LogError($"{nameof(NotesContext)}failed opening file. "+file.FullName);
                }
            }
        }

        internal NoteBundle getNoteBundle()
        {

            if (Bundles.Count == 0) return null;
                return Bundles[Bundles.Count - 1];
        }


        internal void AddNote(string clipText)
        {
            int maxID = getMaxID();

            Note newNote = new Note() { noteText = clipText, noteCreated = DateTime.Now, ID = maxID + 1 };
            AddNote(newNote);
        }

        internal void AddNote(string clipText, DateTime date)
        {
            int maxID = getMaxID();

            Note newNote = new Note() { noteText = clipText, noteCreated = date, ID = maxID + 1 };
            AddNote(newNote);
        }

        internal void AddNote(Note note)
        {
            if (Bundles.Count == 0)
            {
                string path = Settings.getInstallationPath()  + filePath + "Notes0.dat";
                Bundles.Add(new NoteBundle(path,0));
            }

            NoteBundle bnd = Bundles[Bundles.Count - 1];

            if(bnd.Count == NoteBundle.BUNDLE_MAX_SIZE)
            {
                string path = Settings.getInstallationPath()  + filePath + "Notes"+ Bundles.Count + ".dat";
                Bundles.Add(new NoteBundle(path, Bundles.Count));
            }

            bnd = Bundles[Bundles.Count - 1];

            bnd.NewNote(note);
        }

        private int getMaxID()
        {
            int max = -1;
            foreach(NoteBundle bnd in Bundles)
            {
                if (max < bnd.CurrMaxID)
                    max = bnd.CurrMaxID;
            }
            return max;
        }

        public Note getNote(int ID)
        {
            Note output = null;
            //smart search
            foreach (NoteBundle bnd in Bundles)
            {
                if (ID > bnd.CurrMaxID)
                    continue;

                output = bnd.getNote(ID);
            }

            if (output != null)
                return output;

            //dumb search
            foreach (NoteBundle bnd in Bundles)
            {
                output = bnd.getNote(ID);

                if (output != null)
                    return output;
            }

            return output;
        }

        internal void wipeData()
        {
            foreach (NoteBundle bnd in Bundles)
            {
                File.Delete(bnd.path + "~");
                File.Move(bnd.path, bnd.path + "~");
            }
            Bundles = new List<NoteBundle>();
        }

        /// <summary>
        /// Higher bundle ID the newer the bundle is
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public NoteBundle getNoteBundle(int ID)
        {
            foreach (NoteBundle bnd in Bundles)
            {
                if (bnd.ID == ID)
                    return bnd;
            }
            return null;
        }
    }
}

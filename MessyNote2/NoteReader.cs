﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessyNote2
{
    class NoteReader : Helper, IHelping
    {
        NotesContext context;
        NoteLister noteDialog;
        Thread dialogThread;

        public NoteReader(NotesContext context)
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.Alt | Keys.N) };
            this.context = context;
            noteDialog = null;
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(NoteReader)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(NoteReader)} Combination doesn't match.");
                return false;
            }


            if (noteDialog != null)
            {
                Logger.LogWarn($"{nameof(NoteReader)} already opened.");
                return true;
            }
            try
            {
                dialogThread = new Thread(new ThreadStart(showNoteLister));
                dialogThread.SetApartmentState(ApartmentState.STA);
                dialogThread.Start();
            
                dialogThread = null;
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(NoteReader)} " + e.StackTrace);
                return false;
            }

            return true;
        }

        private void showNoteLister()
        {
            noteDialog = new NoteLister(context);
            noteDialog.ShowDialog();
            
            noteDialog = null;
        }

        public override bool suppressKey(LinkedListNode<KeyCombo> pressedKey)
        {
            if (isEnabled())
                return true;
            else
                return false;
        }
    }
}

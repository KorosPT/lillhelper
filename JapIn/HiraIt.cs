﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// Helper that takes romaji text in the clipboard and writes it in hiragana
    /// </summary>
    class HiraIt : Helper, IHelping
    {
        private RomajiHiraKataTranslator translator;

        public HiraIt(RomajiHiraKataTranslator translator)
        {
            this.translator = translator;
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.X), new KeyCombo(Keys.Control | Keys.H) };
        }


        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(HiraIt)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(HiraIt)} Combination doesn't match.");
                return false;
            }

            //Get text i clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }
                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            //Translate text to Hiragana
            String translatedText = translator.Convert(clipText, RomajiHiraKataTranslator.Mode.Hiragana);

            System.Threading.Thread.Sleep(500);
            SendKeys.SendWait(translatedText);

            return true;
        }
    }
}

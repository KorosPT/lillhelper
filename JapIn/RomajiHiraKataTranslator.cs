﻿using JapIn.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JapIn
{
    /// <summary>
    /// Taken from https://www.codeproject.com/Tips/792588/How-to-Convert-between-Romaji-and-Hiragana-Katakan
    /// </summary>
    public class RomajiHiraKataTranslator
    {
        private List<string> Database = new List<string>();

        public enum Mode
        {
            Hiragana,
            Katakana,
            Romaji
        }

        public RomajiHiraKataTranslator()
        {
            GetDatabase();
        }

        private void GetDatabase()
        {
           // using (System.IO.StreamReader sr = new System.IO.StreamReader(Resources.Database))
            {
                //while (!sr.EndOfStream)
                foreach(String line in Resources.Database.Replace("\r","").Split('\n'))
                {
                    //string splitMe = sr.ReadLine();
                    Database.Add(line);
                }
            }
        }

        public static string NormalizeStringForUrl(string name)
        {
            String normalizedString = name.Normalize(NormalizationForm.FormD);
            normalizedString.Replace((char)769, '-');

            StringBuilder stringBuilder = new StringBuilder();

            foreach (char c in normalizedString.ToCharArray())
            {
                switch (CharUnicodeInfo.GetUnicodeCategory(c))
                {
                    case UnicodeCategory.LowercaseLetter:
                    case UnicodeCategory.UppercaseLetter:
                    case UnicodeCategory.DecimalDigitNumber:
                        stringBuilder.Append(c);
                        break;
                    case UnicodeCategory.SpaceSeparator:
                    case UnicodeCategory.ConnectorPunctuation:
                    case UnicodeCategory.DashPunctuation:
                        stringBuilder.Append('_');
                        break;
                }
                if (c == 769)
                    stringBuilder.Append('-');
            }
            string result = stringBuilder.ToString();
            return String.Join("_", result.Split(new char[] { '_' }
                , StringSplitOptions.RemoveEmptyEntries)); // remove duplicate underscores
        }

        public string Convert(string text, Mode convertMode)
        {
            text = text.ToLower();
            text = text.Replace("č", "ch");
            text = text.Replace("š", "sh");
            text = text.Replace("ů", "u-"); // ů
            text = text.Normalize(NormalizationForm.FormD).Replace((char)769, '-'); //á, ú, é...
            NormalizeStringForUrl(text);

            string roma = string.Empty;
            string hira = string.Empty;
            string kata = string.Empty;

            foreach (string row in Database)
            {
                var split = row.Split('@');
                roma = split[0].Normalize(NormalizationForm.FormD);
                hira = split[1];
                kata = split[2];

                switch (convertMode)
                {
                    case Mode.Romaji:
                        text = text.Replace(hira, roma);
                        text = text.Replace(kata, roma.ToUpper());
                        break;
                    case Mode.Hiragana:
                        text = text.Replace(roma, hira);
                        break;
                    case Mode.Katakana:
                        text = text.Replace(roma, kata);
                        break;
                }
            }

            return text;
        }

    }
}

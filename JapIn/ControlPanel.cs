﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// Settings panel for JapIn key manager
    /// </summary>
    public partial class ControlPanel : UserControl
    {
        JapIn japit;
        public ControlPanel(JapIn japit)
        {
            InitializeComponent();
            this.japit = japit;
            fillData();
        }

        private void fillData()
        {
            chk_hira.Checked = japit.getHelpers()[0].isEnabled();
            chk_kata.Checked = japit.getHelpers()[1].isEnabled();
            chk_exp1.Checked = japit.getHelpers()[2].isEnabled();

            txt_head.Text = japit.getFormater().header;
            txt_inReg.Text = japit.getFormater().inputRegex;
            txt_outReg.Text = japit.getFormater().outputRegex;
            txt_map.Text = string.Join(" ", japit.getFormater().mapping);
            txt_mod.Text = string.Join(" ", japit.getFormater().modding);
        }

        private void hira_CheckedChanged(object sender, EventArgs e)
        {
            japit.getHelpers()[0].setEnabled(chk_hira.Checked);
        }

        private void kata_CheckedChanged(object sender, EventArgs e)
        {
            japit.getHelpers()[1].setEnabled(chk_kata.Checked);
        }

        private void exp1_CheckedChanged(object sender, EventArgs e)
        {
            japit.getHelpers()[2].setEnabled(chk_exp1.Checked);
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            japit.getFormater().header = txt_head.Text;
            japit.getFormater().inputRegex = txt_inReg.Text;
            japit.getFormater().outputRegex = txt_outReg.Text;
            
            japit.getFormater().modding = txt_mod.Text.Replace(" ", "").ToCharArray();
            japit.getFormater().mapping = txt_map.Text.Replace(" ", "").ToCharArray().Select(c => Convert.ToInt32(c.ToString())).ToArray();
            //japit.getFormater().mapping = txt_map.Text.Split(' ');
            //txt_mod.Text = string.Join(" ", japit.getFormater().modding);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            japit.setFormaterDefaultSetting();
            fillData();
        }
    }
}

﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// KeyManager containing three IHelping classes.
    /// HiraIt writes romaji in clipoard as hiragana
    /// KataIt writes romaji in clipoard as katakana
    /// Experiment1 writes ů
    /// </summary>
    public class JapIn : IKeyManaging
    {
        private HiraIt hira;
        private KataIt kata;
        //private Experiment1 exp1;
        private TextFormater formater;
        private RomajiHiraKataTranslator translator;


        public String default_header = "Katakana = Hirigana = Romaji = Regular\r\n";
        public char[] default_modding = new char[] { 'k', 'h', '-', '-' };
        public int[] default_mapping = new int[] { 0, 0, 0, 1 };
        public String default_outputRegex = "ñ = ñ = ñ = ñ\n";
        public String default_inputRegex = "=";

        private string formater_header = "JAPIN_FORMATER_HEADER";
        private string formater_modding = "JAPIN_FORMATER_MODDING";
        private string formater_mapping = "JAPIN_FORMATER_MAPPING";
        private string formater_outputRegex = "JAPIN_FORMATER_OUT_REGEX";
        private string formater_inputRegex = "JAPIN_FORMATER_IN_REGEX";


        public JapIn()
        {
            translator = new RomajiHiraKataTranslator();
            hira = new HiraIt(translator);
            kata = new KataIt(translator);
            //exp1 = new Experiment1();
            //exp1.setEnabled(false);
            formater = new TextFormater(translator);

            loadSettings();
        }

        private void loadSettings()
        {
            string header = Settings.getSettingValue(formater_header, default_header);
            string modding = Settings.getSettingValue(formater_modding, string.Join(" ", default_modding) );
            string mapping = Settings.getSettingValue(formater_mapping, string.Join(" ", default_mapping) );
            string outRegex = Settings.getSettingValue(formater_outputRegex, default_outputRegex);
            string inRegex = Settings.getSettingValue(formater_inputRegex, default_inputRegex);

            formater.header = header;
            formater.modding =modding.Replace(" ","").ToCharArray();
            formater.mapping = mapping.Replace(" ","").ToCharArray().Select(c => Convert.ToInt32(c.ToString())).ToArray();
            formater.outputRegex = outRegex;
            formater.inputRegex = inRegex;
        }

        public void setFormaterDefaultSetting()
        {
            formater.header = default_header;
            formater.modding = default_modding;
            formater.mapping = default_mapping;
            formater.outputRegex = default_outputRegex;
            formater.inputRegex = default_inputRegex;

            Settings.setSettingValue(formater_header, default_header);
            Settings.setSettingValue(formater_modding, string.Join(" ", default_modding));
            Settings.setSettingValue(formater_mapping, string.Join(" ", default_mapping));
            Settings.setSettingValue(formater_outputRegex, default_outputRegex);
            Settings.setSettingValue(formater_inputRegex, default_inputRegex);
        }

        public IHelping[] getHelpers()
        {
            return new IHelping[] { hira, kata,/* exp1, */formater };
        }

        public TextFormater getFormater()
        {
            return formater;
        }

        public String getBriefDescription()
        {
            return "Romaji to Kana";
        }

        public System.Windows.Forms.UserControl getControlPanel()
        {
            return new ControlPanel(this);
        }

        public override string ToString()
        {
            return getBriefDescription();
        }

        public MenuItem[] getMenuButtons()
        {
            return new MenuItem[0];
        }
    }
}

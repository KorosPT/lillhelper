﻿namespace JapIn
{
    partial class ControlPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.chk_hira = new System.Windows.Forms.CheckBox();
            this.chk_kata = new System.Windows.Forms.CheckBox();
            this.chk_exp1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_inReg = new System.Windows.Forms.TextBox();
            this.txt_outReg = new System.Windows.Forms.TextBox();
            this.txt_map = new System.Windows.Forms.TextBox();
            this.txt_mod = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_head = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Location = new System.Drawing.Point(4, 4);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(141, 17);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "JapIt - wrting settings";
            // 
            // chk_hira
            // 
            this.chk_hira.AutoSize = true;
            this.chk_hira.Location = new System.Drawing.Point(15, 28);
            this.chk_hira.Name = "chk_hira";
            this.chk_hira.Size = new System.Drawing.Size(151, 21);
            this.chk_hira.TabIndex = 1;
            this.chk_hira.Text = "Romaji to Hiragana";
            this.chk_hira.UseVisualStyleBackColor = true;
            this.chk_hira.CheckedChanged += new System.EventHandler(this.hira_CheckedChanged);
            // 
            // chk_kata
            // 
            this.chk_kata.AutoSize = true;
            this.chk_kata.Location = new System.Drawing.Point(15, 56);
            this.chk_kata.Name = "chk_kata";
            this.chk_kata.Size = new System.Drawing.Size(153, 21);
            this.chk_kata.TabIndex = 2;
            this.chk_kata.Text = "Romaji to Katakana";
            this.chk_kata.UseVisualStyleBackColor = true;
            this.chk_kata.CheckedChanged += new System.EventHandler(this.kata_CheckedChanged);
            // 
            // chk_exp1
            // 
            this.chk_exp1.AutoSize = true;
            this.chk_exp1.Location = new System.Drawing.Point(15, 84);
            this.chk_exp1.Name = "chk_exp1";
            this.chk_exp1.Size = new System.Drawing.Size(123, 21);
            this.chk_exp1.TabIndex = 3;
            this.chk_exp1.Text = "\'U\' with a circle";
            this.chk_exp1.UseVisualStyleBackColor = true;
            this.chk_exp1.CheckedChanged += new System.EventHandler(this.exp1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "InReg";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "OutReg";
            // 
            // txt_inReg
            // 
            this.txt_inReg.Location = new System.Drawing.Point(60, 118);
            this.txt_inReg.Name = "txt_inReg";
            this.txt_inReg.Size = new System.Drawing.Size(100, 22);
            this.txt_inReg.TabIndex = 6;
            // 
            // txt_outReg
            // 
            this.txt_outReg.Location = new System.Drawing.Point(60, 147);
            this.txt_outReg.Name = "txt_outReg";
            this.txt_outReg.Size = new System.Drawing.Size(100, 22);
            this.txt_outReg.TabIndex = 7;
            // 
            // txt_map
            // 
            this.txt_map.Enabled = false;
            this.txt_map.Location = new System.Drawing.Point(235, 147);
            this.txt_map.Name = "txt_map";
            this.txt_map.Size = new System.Drawing.Size(100, 22);
            this.txt_map.TabIndex = 11;
            // 
            // txt_mod
            // 
            this.txt_mod.Enabled = false;
            this.txt_mod.Location = new System.Drawing.Point(235, 115);
            this.txt_mod.Name = "txt_mod";
            this.txt_mod.Size = new System.Drawing.Size(100, 22);
            this.txt_mod.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Mapping";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(167, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Modding";
            // 
            // txt_head
            // 
            this.txt_head.Location = new System.Drawing.Point(60, 175);
            this.txt_head.Name = "txt_head";
            this.txt_head.Size = new System.Drawing.Size(100, 22);
            this.txt_head.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Header";
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(170, 178);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 14;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(251, 178);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Default";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.txt_head);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_map);
            this.Controls.Add(this.txt_mod);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_outReg);
            this.Controls.Add(this.txt_inReg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chk_exp1);
            this.Controls.Add(this.chk_kata);
            this.Controls.Add(this.chk_hira);
            this.Controls.Add(this.lbl_title);
            this.Name = "ControlPanel";
            this.Size = new System.Drawing.Size(359, 229);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.CheckBox chk_hira;
        private System.Windows.Forms.CheckBox chk_kata;
        private System.Windows.Forms.CheckBox chk_exp1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_inReg;
        private System.Windows.Forms.TextBox txt_outReg;
        private System.Windows.Forms.TextBox txt_map;
        private System.Windows.Forms.TextBox txt_mod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_head;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button button1;
    }
}

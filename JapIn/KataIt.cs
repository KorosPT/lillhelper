﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// Helper that takes romaji text in the clipboard and writes it in katakana
    /// </summary>
    class KataIt : Helper, IHelping
    {
        private RomajiHiraKataTranslator translator;

        public KataIt(RomajiHiraKataTranslator translator)
        {
            this.translator = translator;
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.X), new KeyCombo(Keys.Control | Keys.K) };
        }
        
        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(KataIt)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(KataIt)} Combination doesn't match.");
                return false;
            }

            //Get text in clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }

                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            //Translate text to Katakana
            String translatedText = translator.Convert(clipText, RomajiHiraKataTranslator.Mode.Katakana);

            System.Threading.Thread.Sleep(500);
            SendKeys.SendWait(translatedText);

            return true;
        }
    }
}

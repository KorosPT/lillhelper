﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// Simple JapIn experiment. On Ctrl + U writes ů
    /// </summary>
    class Experiment1 : Helper, IHelping
    {

        public Experiment1()
        {
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.OemMinus), new KeyCombo(Keys.U) };
        }

        bool enabled = true;
        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        public bool processKeyStroke(List<KeyCombo> recentlyPressedKeys)
        {
            if (!enabled) return false;
            Logger.LogInfo($"{nameof(Experiment1)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(Experiment1)} Combination doesn't match.");
                return false;
            }

            System.Threading.Thread.Sleep(100); //Sleep so the writing is not canceled by user still holding Ctrl
            SendKeys.SendWait("ů");

            return true;
        }

        public bool suppressHey()
        {
            return false;
        }
    }
}

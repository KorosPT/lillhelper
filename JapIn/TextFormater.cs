﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JapIn
{
    /// <summary>
    /// Simple JapIn experiment. On Ctrl + U writes ů
    /// </summary>
    public class TextFormater : Helper, IHelping
    {
        private RomajiHiraKataTranslator translator;
        public String inputRegex { get; set; }
        public String header { get; set; }
        public char[] modding { get; set; } 
        public int[] mapping { get; set; }
        public String outputRegex { get; set; }

        public TextFormater(RomajiHiraKataTranslator translator)
        {
            this.translator = translator;
            keyBinding = new KeyCombo[] { new KeyCombo(Keys.Control | Keys.C), new KeyCombo(Keys.Control | Keys.Alt | Keys.F) };
        }

        public bool processKeyStroke(LinkedListNode<KeyCombo> recentlyPressedKeys)
        {
            if (!isEnabled()) return false;
            Logger.LogInfo($"{nameof(TextFormater)} called.");

            if (!combinationTriggered(recentlyPressedKeys))
            {
                Logger.LogInfo($"{nameof(TextFormater)} Combination doesn't match.");
                return false;
            }

            //Get text in clipboard
            String clipText = "";
            Exception threadEx = null;
            Thread staThread = new Thread(
                delegate ()
                {
                    try
                    {
                        clipText = Clipboard.GetText();
                    }

                    catch (Exception ex)
                    {
                        threadEx = ex;
                    }
                });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();

            if (string.IsNullOrEmpty(clipText)) return false;

            try
            {
                String output = header;

                foreach (String line in clipText.Split('\n'))
                {
                    String[] matches = Regex.Split(line.Replace('ú', 'ů'), inputRegex);

                    output += outputRegex;

                    for (int i = 0; i < modding.Length; i++)
                    {
                        if (modding[i] == '-')
                            output = output.Insert(output.IndexOf('ñ'), matches[mapping[i]]);
                        else if (modding[i] == 'k')
                            output = output.Insert(output.IndexOf('ñ'), translator.Convert(matches[mapping[i]], RomajiHiraKataTranslator.Mode.Katakana));
                        else if (modding[i] == 'h')
                            output = output.Insert(output.IndexOf('ñ'), translator.Convert(matches[mapping[i]], RomajiHiraKataTranslator.Mode.Hiragana));

                        output = output.Remove(output.IndexOf('ñ'), 1);
                        output.Replace('ñ', ' ');
                    }
                }

                //Special characters of SendKeys have to be bracketed
                output = Regex.Replace(output, "[+^%~()]", "{$0}");

                System.Threading.Thread.Sleep(500);
                SendKeys.SendWait(output);
            }
            catch (Exception e)
            {
                Logger.LogInfo($"{nameof(TextFormater)} Formating doesn't match.");
                MessageBox.Show($"{nameof(TextFormater)} Formating doesn't match.");

            }
            return true;
        }
    }
}

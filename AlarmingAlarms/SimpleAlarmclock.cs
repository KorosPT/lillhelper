﻿using AlarmingAlarms.Properties;
using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmingAlarms
{
    /// <summary>
    /// ITiming class that rings at specific time
    /// </summary>
    public class SimpleAlarmclock : ITiming
    {
        /// <summary>
        /// List of times to rings at
        /// </summary>
        List<DateTime> wakeUps;
        /// <summary>
        /// List of times each individual alarm was last sounded
        /// </summary>
        List<DateTime> triggered;
        
        public int Volume { get; set; } = 4;   // 0 - 10
        public Sound sound { get; set; }

        public SimpleAlarmclock()
        {
            wakeUps = new List<DateTime>();
            triggered = new List<DateTime>();
        }

        public void addAlarm(DateTime alarm)
        {
            wakeUps.Add(alarm);
            triggered.Add(alarm.AddMinutes(-5));
        }

        public void addAlarm(int hour, int minute)
        {
            //Just one alarm so far (GUI limitation)
            wakeUps = new List<DateTime>();
            triggered = new List<DateTime>();
            wakeUps.Add(new DateTime(10,10,10,hour,minute,10));
            triggered.Add(wakeUps[wakeUps.Count-1].AddMinutes(-6));
        }

        public bool isPeriodical()
        {
            return false;
        }

        bool enabled = false;
        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
            if(!enabled)
            {
                wakeUps = new List<DateTime>();
                triggered = new List<DateTime>();
            }
        }


        public void trigger()
        {
            if (!isEnabled()) return;
            //foreach (DateTime wake in wakeUps)
            for(int i = 0; i < wakeUps.Count; i++)
            {
                try { 
                if(triggered[i] == null || (DateTime.Now - triggered[i]).Minutes > 5)
                if (DateTime.Now.Hour == wakeUps[i].Hour && DateTime.Now.Minute == wakeUps[i].Minute)
                    {
                        Logger.LogInfo($"{nameof(SimpleAlarmclock)} ringing.");

                        if (sound.isPredefined)
                            Amenities.SoundPlayer.PlaySound(sound.predefSound, Volume);
                        else
                            Amenities.SoundPlayer.PlaySound(sound.soundPath, Volume);

                        Amenities.NotificationWindow.getNotificationDialog("Alarm!! Time to wake up!!");
                        triggered[i] = new DateTime(DateTime.Now.Ticks);
                }
            } catch (Exception e)
            {
                Logger.LogError($"{nameof(SimpleAlarmclock)} crashed while ringing. Disabled. " + e.Message);
                enabled = false;
            }
        }
        }

        internal DateTime? getAlarm()
        {
            if (wakeUps != null && wakeUps.Count > 0)
                return wakeUps[wakeUps.Count - 1];
            return null;
        }
    }
}

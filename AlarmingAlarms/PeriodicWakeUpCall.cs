﻿using AlarmingAlarms.Properties;
using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmingAlarms
{
    /// <summary>
    /// ITiming class that makes a sound every hour
    /// </summary>
    public class PeriodicWakeUpCall : ITiming
    {
        //Run every hour?
        bool resetEveryHour;
        /// <summary>
        /// Specific minutes it should ring at
        /// </summary>
        int minutes;
        /// <summary>
        /// Already rang
        /// </summary>
        bool triggered = false;

        public  int Volume { get; set; } = 4;   // 0 - 10
        public Sound sound { get; set; }

        [DllImport("winmm.dll")]
        public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

        [DllImport("winmm.dll")]
        public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);

        public PeriodicWakeUpCall()
        {
            resetEveryHour = true;
            minutes = 0;
        }

        public bool isPeriodical()
        {
            //Contrary to what would make sense, periodicWakeUp is not periodic....
            //Periodic in this context means (counting down to something)
            return false;
        }

        bool enabled = true;

        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        public void trigger()
        {
            if (!enabled) return;

            if (triggered && DateTime.Now.Minute == minutes + 1 && resetEveryHour)
                triggered = false;

            if(!triggered && DateTime.Now.Minute == minutes)
            {
                Logger.LogInfo($"{nameof(PeriodicWakeUpCall)} ringing.");

                if (sound.isPredefined)
                    Amenities.SoundPlayer.PlaySound(sound.predefSound, Volume);
                else
                    Amenities.SoundPlayer.PlaySound(sound.soundPath, Volume);

                triggered = true;
            }
        }
    }
}

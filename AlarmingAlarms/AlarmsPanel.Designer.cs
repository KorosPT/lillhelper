﻿namespace AlarmingAlarms
{
    partial class AlarmsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            this.enabled = false;
            this.refreshingThread.Abort();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.chk_hourlyWakeUp = new System.Windows.Forms.CheckBox();
            this.chk_alarm = new System.Windows.Forms.CheckBox();
            this.chk_timer = new System.Windows.Forms.CheckBox();
            this.num_timerMin = new System.Windows.Forms.NumericUpDown();
            this.num_timerHour = new System.Windows.Forms.NumericUpDown();
            this.lbl_h2 = new System.Windows.Forms.Label();
            this.lbl_min2 = new System.Windows.Forms.Label();
            this.lbl_countdown = new System.Windows.Forms.Label();
            this.lbl_timerTIme = new System.Windows.Forms.Label();
            this.lbl_min1 = new System.Windows.Forms.Label();
            this.lbl_h1 = new System.Windows.Forms.Label();
            this.num_alarmHour = new System.Windows.Forms.NumericUpDown();
            this.num_AlatmMin = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.btn_setSound = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_timerMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_timerHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_alarmHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_AlatmMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Location = new System.Drawing.Point(4, 4);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(161, 17);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "AlarmingAlarms Settings";
            // 
            // chk_hourlyWakeUp
            // 
            this.chk_hourlyWakeUp.AutoSize = true;
            this.chk_hourlyWakeUp.Location = new System.Drawing.Point(20, 31);
            this.chk_hourlyWakeUp.Name = "chk_hourlyWakeUp";
            this.chk_hourlyWakeUp.Size = new System.Drawing.Size(127, 21);
            this.chk_hourlyWakeUp.TabIndex = 1;
            this.chk_hourlyWakeUp.Text = "Hourly wake up";
            this.chk_hourlyWakeUp.UseVisualStyleBackColor = true;
            this.chk_hourlyWakeUp.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // chk_alarm
            // 
            this.chk_alarm.AutoSize = true;
            this.chk_alarm.Location = new System.Drawing.Point(20, 59);
            this.chk_alarm.Name = "chk_alarm";
            this.chk_alarm.Size = new System.Drawing.Size(102, 21);
            this.chk_alarm.TabIndex = 2;
            this.chk_alarm.Text = "Alarm clock";
            this.chk_alarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chk_alarm.UseVisualStyleBackColor = true;
            this.chk_alarm.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chk_timer
            // 
            this.chk_timer.AutoSize = true;
            this.chk_timer.Location = new System.Drawing.Point(20, 87);
            this.chk_timer.Name = "chk_timer";
            this.chk_timer.Size = new System.Drawing.Size(66, 21);
            this.chk_timer.TabIndex = 3;
            this.chk_timer.Text = "Timer";
            this.chk_timer.UseVisualStyleBackColor = true;
            this.chk_timer.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // num_timerMin
            // 
            this.num_timerMin.Location = new System.Drawing.Point(211, 87);
            this.num_timerMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.num_timerMin.Name = "num_timerMin";
            this.num_timerMin.Size = new System.Drawing.Size(48, 22);
            this.num_timerMin.TabIndex = 4;
            this.num_timerMin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num_timerMin.ValueChanged += new System.EventHandler(this.num_timerMin_ValueChanged);
            // 
            // num_timerHour
            // 
            this.num_timerHour.Location = new System.Drawing.Point(128, 86);
            this.num_timerHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.num_timerHour.Name = "num_timerHour";
            this.num_timerHour.Size = new System.Drawing.Size(55, 22);
            this.num_timerHour.TabIndex = 5;
            this.num_timerHour.ValueChanged += new System.EventHandler(this.num_timerHour_ValueChanged);
            // 
            // lbl_h2
            // 
            this.lbl_h2.AutoSize = true;
            this.lbl_h2.Location = new System.Drawing.Point(189, 91);
            this.lbl_h2.Name = "lbl_h2";
            this.lbl_h2.Size = new System.Drawing.Size(16, 17);
            this.lbl_h2.TabIndex = 6;
            this.lbl_h2.Text = "h";
            // 
            // lbl_min2
            // 
            this.lbl_min2.AutoSize = true;
            this.lbl_min2.Location = new System.Drawing.Point(265, 92);
            this.lbl_min2.Name = "lbl_min2";
            this.lbl_min2.Size = new System.Drawing.Size(30, 17);
            this.lbl_min2.TabIndex = 7;
            this.lbl_min2.Text = "min";
            // 
            // lbl_countdown
            // 
            this.lbl_countdown.AutoSize = true;
            this.lbl_countdown.Location = new System.Drawing.Point(17, 154);
            this.lbl_countdown.Name = "lbl_countdown";
            this.lbl_countdown.Size = new System.Drawing.Size(120, 17);
            this.lbl_countdown.TabIndex = 8;
            this.lbl_countdown.Text = "Timer countdown:";
            this.lbl_countdown.Visible = false;
            // 
            // lbl_timerTIme
            // 
            this.lbl_timerTIme.AutoSize = true;
            this.lbl_timerTIme.Location = new System.Drawing.Point(141, 154);
            this.lbl_timerTIme.Name = "lbl_timerTIme";
            this.lbl_timerTIme.Size = new System.Drawing.Size(0, 17);
            this.lbl_timerTIme.TabIndex = 9;
            // 
            // lbl_min1
            // 
            this.lbl_min1.AutoSize = true;
            this.lbl_min1.Location = new System.Drawing.Point(265, 65);
            this.lbl_min1.Name = "lbl_min1";
            this.lbl_min1.Size = new System.Drawing.Size(30, 17);
            this.lbl_min1.TabIndex = 13;
            this.lbl_min1.Text = "min";
            // 
            // lbl_h1
            // 
            this.lbl_h1.AutoSize = true;
            this.lbl_h1.Location = new System.Drawing.Point(189, 64);
            this.lbl_h1.Name = "lbl_h1";
            this.lbl_h1.Size = new System.Drawing.Size(16, 17);
            this.lbl_h1.TabIndex = 12;
            this.lbl_h1.Text = "h";
            // 
            // num_alarmHour
            // 
            this.num_alarmHour.Location = new System.Drawing.Point(128, 59);
            this.num_alarmHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.num_alarmHour.Name = "num_alarmHour";
            this.num_alarmHour.Size = new System.Drawing.Size(55, 22);
            this.num_alarmHour.TabIndex = 11;
            this.num_alarmHour.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num_alarmHour.ValueChanged += new System.EventHandler(this.num_alarmHour_ValueChanged);
            // 
            // num_AlatmMin
            // 
            this.num_AlatmMin.Location = new System.Drawing.Point(211, 60);
            this.num_AlatmMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.num_AlatmMin.Name = "num_AlatmMin";
            this.num_AlatmMin.Size = new System.Drawing.Size(48, 22);
            this.num_AlatmMin.TabIndex = 10;
            this.num_AlatmMin.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num_AlatmMin.ValueChanged += new System.EventHandler(this.num_AlatmMin_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Volume:";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(84, 115);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(202, 56);
            this.trackBar1.TabIndex = 15;
            this.trackBar1.Value = 4;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // btn_setSound
            // 
            this.btn_setSound.Location = new System.Drawing.Point(268, 4);
            this.btn_setSound.Name = "btn_setSound";
            this.btn_setSound.Size = new System.Drawing.Size(86, 26);
            this.btn_setSound.TabIndex = 16;
            this.btn_setSound.Text = "Set sound";
            this.btn_setSound.UseVisualStyleBackColor = true;
            this.btn_setSound.Click += new System.EventHandler(this.btn_setSound_Click);
            // 
            // AlarmsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_setSound);
            this.Controls.Add(this.lbl_timerTIme);
            this.Controls.Add(this.lbl_countdown);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_min1);
            this.Controls.Add(this.lbl_h1);
            this.Controls.Add(this.num_alarmHour);
            this.Controls.Add(this.num_AlatmMin);
            this.Controls.Add(this.lbl_min2);
            this.Controls.Add(this.lbl_h2);
            this.Controls.Add(this.num_timerHour);
            this.Controls.Add(this.num_timerMin);
            this.Controls.Add(this.chk_timer);
            this.Controls.Add(this.chk_alarm);
            this.Controls.Add(this.chk_hourlyWakeUp);
            this.Controls.Add(this.lbl_title);
            this.Name = "AlarmsPanel";
            this.Size = new System.Drawing.Size(357, 195);
            this.Load += new System.EventHandler(this.AlarmsPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.num_timerMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_timerHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_alarmHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_AlatmMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.CheckBox chk_hourlyWakeUp;
        private System.Windows.Forms.CheckBox chk_alarm;
        private System.Windows.Forms.CheckBox chk_timer;
        private System.Windows.Forms.NumericUpDown num_timerMin;
        private System.Windows.Forms.NumericUpDown num_timerHour;
        private System.Windows.Forms.Label lbl_h2;
        private System.Windows.Forms.Label lbl_min2;
        private System.Windows.Forms.Label lbl_countdown;
        private System.Windows.Forms.Label lbl_timerTIme;
        private System.Windows.Forms.Label lbl_min1;
        private System.Windows.Forms.Label lbl_h1;
        private System.Windows.Forms.NumericUpDown num_alarmHour;
        private System.Windows.Forms.NumericUpDown num_AlatmMin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button btn_setSound;
    }
}

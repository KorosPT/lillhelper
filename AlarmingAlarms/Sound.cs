﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmingAlarms
{
    public class Sound
    {
        public bool isPredefined { get; set; } = true;
        public System.IO.UnmanagedMemoryStream predefSound { get; private set; } = Properties.Resources.Alarm;
        public string PredefName = "";
        public string soundPath { get; private set; } = "";

        private static Dictionary<string, System.IO.UnmanagedMemoryStream> resourceToNameDic;

        public void SetPredeffedSOundsByName(String name)
        {
            if (ResourceToNameDic.ContainsKey(name))
            {
                isPredefined = true;
                PredefName = name;
                predefSound = ResourceToNameDic[name];
            }
            else
            {
                name = "Alarm";
                isPredefined = true;
                PredefName = name;
                predefSound = ResourceToNameDic[name];
            }
            soundPath = "";
        }

        public void SetSoundByPath(String path)
        {
            isPredefined = false;
            soundPath = path;
            PredefName = "";
        }

        public static Dictionary<string, System.IO.UnmanagedMemoryStream> ResourceToNameDic
        {
            get
            {
                if (resourceToNameDic == null)
                {
                    resourceToNameDic = new Dictionary<string, System.IO.UnmanagedMemoryStream>();
                    resourceToNameDic.Add("Alarm", Properties.Resources.Alarm);
                    resourceToNameDic.Add("Alarm2", Properties.Resources.Alarm2);
                    resourceToNameDic.Add("foghorn", Properties.Resources.foghorn);
                    resourceToNameDic.Add("maggots", Properties.Resources.maggots);
                }
                return resourceToNameDic;
            }
        }

        public static System.IO.UnmanagedMemoryStream[] getPredefinedSounds()
        {
            return ResourceToNameDic.Values.ToArray();
        }


        public static string GetNameOf(System.IO.UnmanagedMemoryStream resource)
        {

            foreach (var v in ResourceToNameDic)
            {
                if (v.Value.Equals(resource))
                {
                    return v.Key;
                }
            }

            return "";
        }

    }
}

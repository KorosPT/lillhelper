﻿using AlarmingAlarms.Properties;
using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmingAlarms
{
    /// <summary>
    /// Simple timer doing countdown
    /// </summary>
    public class SimpleTimer : ITiming
    {
        DateTime ringAt;
        DateTime startAt;
        AlarmingAlarms alarms;
        public Sound sound { get; set; }
        bool triggered = false;

        int minutes = 0;
        int hours = 0;

        public int Volume { get; set; } = 4;   // 0 - 10

        public SimpleTimer(AlarmingAlarms alarms)
        {
            this.alarms = alarms;
            triggered = true;
        }

        public void startTimer(int hurs, int minutes)
        {
            this.minutes = minutes;
            this.hours = hurs;
            triggered = false;
            ringAt = new DateTime(DateTime.Now.Ticks).AddHours(hurs).AddMinutes(minutes);
            startAt = new DateTime(DateTime.Now.Ticks);
        }

        public int getMinutes()
        { return minutes; }

        public int getHours()
        { return hours; }

        public bool isPeriodical()
        {
           return true;
        }

        bool enabled = false;
        public bool isEnabled()
        {
            return enabled;
        }
        public void setEnabled(bool enabled)
        {
            this.enabled = enabled;
            //alarms.setTimerLabelVisible(enabled);
        }

        public void trigger()
        {
            if (!isEnabled()) return;
            if(!triggered)
            {
                try { 
                    startAt = startAt.AddMilliseconds(Settings.tickPeriod);
                    if (startAt.Hour == ringAt.Hour && startAt.Minute == ringAt.Minute && (ringAt.Second - startAt.Second) <= 3)
                    {
                        Logger.LogInfo($"{nameof(SimpleTimer)} ringing.");

                        if (sound.isPredefined)
                            Amenities.SoundPlayer.PlaySound(sound.predefSound, Volume);
                        else
                            Amenities.SoundPlayer.PlaySound(sound.soundPath, Volume);

                        Amenities.NotificationWindow.getNotificationDialog("Time is up up!!");

                        triggered = true;
                        enabled = false;
                    }
                } catch (Exception e)
                {
                    Logger.LogError($"{nameof(SimpleTimer)} crashed while ringing. Disabled. "+e.Message);
                    triggered = true;
                    enabled = false;
                }
                //alarms.tickOccured();
            }
        }

        internal string getRemainingTime()
        {
            return (ringAt - startAt).ToString();
        }
    }
}

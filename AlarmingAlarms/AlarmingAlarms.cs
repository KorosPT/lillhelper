﻿using Amenities;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmingAlarms
{
    /// <summary>
    /// Time manager containing three ITiming
    /// Hourly alarm, alarm and timer
    /// </summary>
    public class AlarmingAlarms : ITimeManaging
    {
        public SimpleAlarmclock alarm { get; }
        public SimpleTimer timer { get; }
        public PeriodicWakeUpCall waker { get; }
        AlarmsPanel settingsPanel;

        private string alarm_volume = "ALARMING_ALARM_VOLUME";
        private string timer_minutes = "ALARMING_ALARM_TIMER_MINUTES";
        private string alarm_minutes = "ALARMING_ALARM_ALARM_MINUTES";
        private string hourly_enabled = "HOURLY_ALARM_ENABLED";

        private string alarm_sound = "ALARMING_ALARM_ALARM_SOUND";
        private string timer_sound = "ALARMING_ALARM_TIMER_SOUND";
        private string waker_sound = "ALARMING_ALARM_WAKER_SOUND";

        private int default_alarm_volume = 2;
        private int default_timer_minutes = 2;
        private int default_alarm_minutes = 8 * 60;
        private bool default_hourly_enabled = true;

        private string default_alarm_sound = "PREDEF Alarm2";
        private string default_timer_sound = "PREDEF Alarm2";
        private string default_waker_sound = "PREDEF Alarm";

        public AlarmingAlarms()
        {
            alarm = new SimpleAlarmclock();
            timer = new SimpleTimer(this);
            waker = new PeriodicWakeUpCall();

            loadSettings();
        }

        private void loadSettings()
        {
            int volume = Settings.getSettingValueInt(alarm_volume, default_alarm_volume);
            setVolume(volume);

            //int timerTime = Settings.getSettingValueInt(timer_minutes, default_timer_minutes);
            //setVolume(volume);

            //int alarmTime = Settings.getSettingValueInt(alarm_minutes, default_alarm_minutes);
            //setVolume(volume);

            bool hourlyEnabled = Settings.getSettingValueBool(hourly_enabled, default_hourly_enabled);
            setPeriodicAlarmEnabled(hourlyEnabled);

            setVolume(volume);

            string soundSet = Settings.getSettingValue(alarm_sound, default_alarm_sound);
            Sound timerSound = new Sound();
            if (soundSet.StartsWith("PREDEF"))
                timerSound.SetPredeffedSOundsByName(soundSet.Substring(7));
            else
                timerSound.SetSoundByPath(soundSet);
            timer.sound = timerSound;

            soundSet = Settings.getSettingValue(timer_sound, default_timer_sound);
            Sound alarmSound = new Sound();
            if (soundSet.StartsWith("PREDEF"))
                alarmSound.SetPredeffedSOundsByName(soundSet.Substring(7));
            else
                alarmSound.SetSoundByPath(soundSet);
            alarm.sound = alarmSound;

            soundSet = Settings.getSettingValue(waker_sound, default_waker_sound);
            Sound perWakSound = new Sound();
            if (soundSet.StartsWith("PREDEF"))
                perWakSound.SetPredeffedSOundsByName(soundSet.Substring(7));
            else
                perWakSound.SetSoundByPath(soundSet);
            waker.sound = perWakSound;
        }

        public void setVolume(int vol)
        {
            waker.Volume = vol;
            alarm.Volume = vol;
            timer.Volume = vol;
            Settings.setSettingValueInt(alarm_volume, vol);
            Settings.saveSettings();
        }

        /// <summary>
        /// Returns the ITiming classes
        /// </summary>
        /// <returns></returns>
        public ITiming[] getTimers()
        {
            return new ITiming[] { alarm, timer, waker };
        }

        internal void saveSoundSettings()
        {

            if (alarm.sound.isPredefined)
                Settings.setSettingValue(alarm_sound, "PREDEF " + alarm.sound.PredefName);
            else
                Settings.setSettingValue(alarm_sound, alarm.sound.soundPath);

            if (timer.sound.isPredefined)
                Settings.setSettingValue(timer_sound, "PREDEF " + timer.sound.PredefName);
            else
                Settings.setSettingValue(timer_sound, timer.sound.soundPath);

            if (waker.sound.isPredefined)
                Settings.setSettingValue(waker_sound, "PREDEF " + waker.sound.PredefName);
            else
                Settings.setSettingValue(waker_sound, waker.sound.soundPath);

            Settings.saveSettings();
        }

        internal SimpleTimer getTimer()
        {
            return timer;
        }

        /// <summary>
        /// Brief fancy name
        /// </summary>
        /// <returns></returns>
        public String getBriefDescription()
        {
            return "Alarms";
        }

        /// <summary>
        /// Returns settings UserCOntrol panel
        /// </summary>
        /// <returns></returns>
        public UserControl getControlPanel()
        {
            settingsPanel = new AlarmsPanel(this);
            settingsPanel.setDefaultTimer(Settings.getSettingValueInt(timer_minutes, default_timer_minutes));
            settingsPanel.setDefaultAlarm(Settings.getSettingValueInt(alarm_minutes, default_alarm_minutes));
            return settingsPanel;
        }

        /// <summary>
        /// Sets an alarn in SimpleAlarmClock
        /// </summary>
        /// <param name="hours"></param>
        /// <param name="minutes"></param>
        internal void addAlarm(int hours, int minutes)
        {
            alarm.addAlarm(hours, minutes);
            Settings.setSettingValueInt(alarm_minutes, (hours * 60) + minutes);
            Settings.saveSettings();

        }

        /// <summary>
        /// Gets current count down value of SimpleTimer
        /// </summary>
        /// <returns></returns>
        internal string getTimerTime()
        {
            return timer.getRemainingTime();
        }

        internal bool isTimerEnabled()
        {
            return timer.isEnabled();
        }

        /// <summary>
        /// Starts a countdown in SimpleTimer
        /// </summary>
        /// <param name="hours"></param>
        /// <param name="minutes"></param>
        public void setTimerTime(int hours, int minutes)
        {
            timer.startTimer(hours, minutes);
            Settings.setSettingValueInt(timer_minutes, (hours * 60) + minutes);
            Settings.saveSettings();
        }

        public void setPeriodicAlarmEnabled(bool enabled)
        {
            waker.setEnabled(enabled);
            Settings.setSettingValueBool(hourly_enabled, enabled);
            Settings.saveSettings();
        }



        ///// <summary>
        ///// Triggered in each tick. Used to refresh lables
        ///// </summary>
        //public void tickOccured()
        //{
        //    if (settingsPanel != null && !settingsPanel.IsDisposed && settingsPanel.IsHandleCreated && settingsPanel.Parent != null)
        //        settingsPanel.updateLabels();
        //}

        //public void setTimerLabelVisible(bool visible)
        //{
        //    if (settingsPanel != null && !settingsPanel.IsDisposed && settingsPanel.IsHandleCreated)
        //        settingsPanel.setTimerLabelVisibele(visible);
        //}

        public DateTime? getLastAlarmTime()
        {
            return alarm.getAlarm();
        }

        public int getVolume()
        {
            return waker.Volume;
        }

        public override string ToString()
        {
            return getBriefDescription();
        }

        public MenuItem[] getMenuButtons()
        {
            return new MenuItem[] { new MenuItem("Tea - 5min", new EventHandler(delegate(object s, EventArgs ev)
                {
                    setTimerTime(0,5);
                    timer.setEnabled(true);
                })) };
        }
    }
}

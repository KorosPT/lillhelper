﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AlarmingAlarms
{
    /// <summary>
    /// Settings panel for AlarmingAlarms
    /// All just simple GIU. Click and reaction to click.
    /// </summary>
    public partial class AlarmsPanel : UserControl
    {
        AlarmingAlarms alarms;
        Thread refreshingThread;
        bool enabled = true;

        //Timeout for method updating the SimpleTimer checkbox, so it doesn't get checked off the moment user checks it
        //Ugly but functional
        int checkboxUpdateSleep = 0;

        public AlarmsPanel(AlarmingAlarms alarms)
        {
            InitializeComponent();
            this.alarms = alarms;
            chk_alarm.Checked = alarms.getTimers()[0].isEnabled();
            chk_timer.Checked = alarms.getTimers()[1].isEnabled();
            chk_hourlyWakeUp.Checked = alarms.getTimers()[2].isEnabled();
            lbl_countdown.Visible = chk_timer.Checked;
            trackBar1.Value = alarms.getVolume();

            if (chk_alarm.Checked && alarms.getLastAlarmTime() != null)
            {
                num_alarmHour.Value = alarms.getLastAlarmTime().Value.Hour;
                num_AlatmMin.Value = alarms.getLastAlarmTime().Value.Minute;
            }

            //this.num_timerHour.ValueChanged += new System.EventHandler(this.num_timerHour_ValueChanged);
            //this.num_timerMin.ValueChanged += new System.EventHandler(this.num_timerMin_ValueChanged);
            //this.chk_timer.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            alarms.setPeriodicAlarmEnabled(chk_hourlyWakeUp.Checked);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (alarms.getTimers()[0].isEnabled() == chk_alarm.Checked) return;
            alarms.getTimers()[0].setEnabled(chk_alarm.Checked);
            alarms.addAlarm((int)num_alarmHour.Value, (int)num_AlatmMin.Value);
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_timer.Checked && !alarms.getTimers()[1].isEnabled())
            {
                checkboxUpdateSleep = 5;
                alarms.getTimers()[1].setEnabled(true);
                alarms.setTimerTime((int)num_timerHour.Value, (int)num_timerMin.Value);
            }
            else if (!chk_timer.Checked && alarms.getTimers()[1].isEnabled())
            {
                checkboxUpdateSleep = 5;
                alarms.getTimers()[1].setEnabled(false);
            }

            //if (alarms.getTimers()[1].isEnabled() == chk_timer.Checked) return;
            //alarms.getTimers()[1].setEnabled(chk_timer.Checked);
            //if (!chk_timer.Checked) return;
            //alarms.setTimerTime((int)num_timerHour.Value, (int)num_timerMin.Value);
        }

        internal void setDefaultTimer(int default_timer)
        {
            if (alarms.getTimers()[1].isEnabled())
            {
                num_timerHour.Value = alarms.getTimer().getHours();
                num_timerMin.Value = alarms.getTimer().getMinutes();
                return;
            }
            num_timerMin.Value = default_timer % 60;
            num_timerHour.Value = (int)default_timer / 60;
        }

        internal void setDefaultAlarm(int default_alarm)
        {
            if (alarms.getTimers()[0].isEnabled()) return;
            num_AlatmMin.Value = default_alarm % 60;
            num_alarmHour.Value = (int)default_alarm / 60;
        }

        public void updateLabels()
        {
            while (enabled)
            {
                Thread.Sleep(100);
                setTimerLabelVisibele();
                //weird formulation because it is called from different thread than it was created in
                lbl_timerTIme.Invoke((MethodInvoker)(() => lbl_timerTIme.Text = alarms.getTimerTime()));
                if (checkboxUpdateSleep == 0 && !alarms.getTimers()[1].isEnabled())
                {
                    chk_timer.Invoke((MethodInvoker)(() => chk_timer.Checked = false));
                }
                else
                    checkboxUpdateSleep--;
            }
        }

        public void setTimerLabelVisibele()
        {
            //weird formulation because it is called from different thread than it was created in
            lbl_timerTIme.Invoke((MethodInvoker)(() => lbl_timerTIme.Visible = alarms.isTimerEnabled()));
            lbl_countdown.Invoke((MethodInvoker)(() => lbl_countdown.Visible = alarms.isTimerEnabled()));
        }

        private void num_AlatmMin_ValueChanged(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
                chk_alarm.Checked = false;
        }

        private void num_alarmHour_ValueChanged(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
                chk_alarm.Checked = false;
        }

        private void num_timerHour_ValueChanged(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
                chk_timer.Checked = false;
        }

        private void num_timerMin_ValueChanged(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
                chk_timer.Checked = false;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            alarms.setVolume(trackBar1.Value);
        }

        private void btn_setSound_Click(object sender, EventArgs e)
        {
            SoundPicker sp = new SoundPicker(alarms);
            sp.Volume = trackBar1.Value;
            sp.ShowDialog();
        }

        private void AlarmsPanel_Load(object sender, EventArgs e)
        {
            refreshingThread = new Thread(new ThreadStart(updateLabels));
            refreshingThread.Start();
        }
    }
}

﻿namespace AlarmingAlarms
{
    partial class SoundPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SoundPicker));
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_periodical = new System.Windows.Forms.RadioButton();
            this.rb_timer = new System.Windows.Forms.RadioButton();
            this.rb_alarm = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_currPref = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_play = new System.Windows.Forms.Button();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.btn_browse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rb_fromFile = new System.Windows.Forms.RadioButton();
            this.rb_predef = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lst_preDef = new System.Windows.Forms.ListBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rb_periodical);
            this.panel1.Controls.Add(this.rb_timer);
            this.panel1.Controls.Add(this.rb_alarm);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(223, 86);
            this.panel1.TabIndex = 0;
            // 
            // rb_periodical
            // 
            this.rb_periodical.AutoSize = true;
            this.rb_periodical.Checked = true;
            this.rb_periodical.Location = new System.Drawing.Point(4, 3);
            this.rb_periodical.Name = "rb_periodical";
            this.rb_periodical.Size = new System.Drawing.Size(122, 21);
            this.rb_periodical.TabIndex = 2;
            this.rb_periodical.TabStop = true;
            this.rb_periodical.Text = "Hourly wakeup";
            this.rb_periodical.UseVisualStyleBackColor = true;
            this.rb_periodical.CheckedChanged += new System.EventHandler(this.rb_periodical_CheckedChanged);
            // 
            // rb_timer
            // 
            this.rb_timer.AutoSize = true;
            this.rb_timer.Location = new System.Drawing.Point(4, 57);
            this.rb_timer.Name = "rb_timer";
            this.rb_timer.Size = new System.Drawing.Size(65, 21);
            this.rb_timer.TabIndex = 1;
            this.rb_timer.Text = "Timer";
            this.rb_timer.UseVisualStyleBackColor = true;
            this.rb_timer.CheckedChanged += new System.EventHandler(this.rb_timer_CheckedChanged);
            // 
            // rb_alarm
            // 
            this.rb_alarm.AutoSize = true;
            this.rb_alarm.Location = new System.Drawing.Point(4, 30);
            this.rb_alarm.Name = "rb_alarm";
            this.rb_alarm.Size = new System.Drawing.Size(65, 21);
            this.rb_alarm.TabIndex = 0;
            this.rb_alarm.Text = "Alarm";
            this.rb_alarm.UseVisualStyleBackColor = true;
            this.rb_alarm.CheckedChanged += new System.EventHandler(this.rb_alarm_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lbl_currPref);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btn_play);
            this.panel2.Controls.Add(this.txt_path);
            this.panel2.Controls.Add(this.btn_browse);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.rb_fromFile);
            this.panel2.Controls.Add(this.rb_predef);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(12, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(223, 251);
            this.panel2.TabIndex = 1;
            // 
            // lbl_currPref
            // 
            this.lbl_currPref.AutoSize = true;
            this.lbl_currPref.Location = new System.Drawing.Point(64, 28);
            this.lbl_currPref.Name = "lbl_currPref";
            this.lbl_currPref.Size = new System.Drawing.Size(40, 17);
            this.lbl_currPref.TabIndex = 8;
            this.lbl_currPref.Text = "none";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Current:";
            // 
            // btn_play
            // 
            this.btn_play.Location = new System.Drawing.Point(48, 209);
            this.btn_play.Name = "btn_play";
            this.btn_play.Size = new System.Drawing.Size(75, 29);
            this.btn_play.TabIndex = 6;
            this.btn_play.Text = "Play";
            this.btn_play.UseVisualStyleBackColor = true;
            this.btn_play.Click += new System.EventHandler(this.btn_play_Click);
            // 
            // txt_path
            // 
            this.txt_path.Location = new System.Drawing.Point(45, 181);
            this.txt_path.Name = "txt_path";
            this.txt_path.Size = new System.Drawing.Size(159, 22);
            this.txt_path.TabIndex = 5;
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(129, 209);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 29);
            this.btn_browse.TabIndex = 4;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Path:";
            // 
            // rb_fromFile
            // 
            this.rb_fromFile.AutoSize = true;
            this.rb_fromFile.Location = new System.Drawing.Point(4, 161);
            this.rb_fromFile.Name = "rb_fromFile";
            this.rb_fromFile.Size = new System.Drawing.Size(83, 21);
            this.rb_fromFile.TabIndex = 2;
            this.rb_fromFile.Text = "From file";
            this.rb_fromFile.UseVisualStyleBackColor = true;
            this.rb_fromFile.CheckedChanged += new System.EventHandler(this.rb_fromFile_CheckedChanged);
            // 
            // rb_predef
            // 
            this.rb_predef.AutoSize = true;
            this.rb_predef.Checked = true;
            this.rb_predef.Location = new System.Drawing.Point(4, 4);
            this.rb_predef.Name = "rb_predef";
            this.rb_predef.Size = new System.Drawing.Size(141, 21);
            this.rb_predef.TabIndex = 1;
            this.rb_predef.TabStop = true;
            this.rb_predef.Text = "Predefined sound";
            this.rb_predef.UseVisualStyleBackColor = true;
            this.rb_predef.CheckedChanged += new System.EventHandler(this.rb_predef_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lst_preDef);
            this.panel3.Location = new System.Drawing.Point(4, 55);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 0;
            // 
            // lst_preDef
            // 
            this.lst_preDef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lst_preDef.ItemHeight = 16;
            this.lst_preDef.Location = new System.Drawing.Point(0, 0);
            this.lst_preDef.Name = "lst_preDef";
            this.lst_preDef.Size = new System.Drawing.Size(200, 100);
            this.lst_preDef.TabIndex = 0;
            this.lst_preDef.SelectedIndexChanged += new System.EventHandler(this.lst_preDef_SelectedIndexChanged);
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(161, 361);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 2;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(80, 361);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 3;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // SoundPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 389);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "SoundPicker";
            this.Text = "SoundPicker";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SoundPicker_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rb_periodical;
        private System.Windows.Forms.RadioButton rb_timer;
        private System.Windows.Forms.RadioButton rb_alarm;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rb_fromFile;
        private System.Windows.Forms.RadioButton rb_predef;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox lst_preDef;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.Button btn_play;
        private System.Windows.Forms.Label lbl_currPref;
        private System.Windows.Forms.Label label2;
    }
}
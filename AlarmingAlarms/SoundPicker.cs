﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Windows.Forms;

namespace AlarmingAlarms
{
    public partial class SoundPicker : Form
    {
        private AlarmingAlarms alarms;

        public int Volume { get; set; } = 4;   // 0 - 10

        Sound tmpAlarm = new Sound();
        Sound tmpTimer = new Sound();
        Sound tmpWaker = new Sound();

        public SoundPicker(AlarmingAlarms alarms)
        {
            this.alarms = alarms;
            InitializeComponent();


            //backup Sounds before modding
            if (alarms.alarm.sound.isPredefined)
                tmpAlarm.SetPredeffedSOundsByName(alarms.alarm.sound.PredefName);
            else
                tmpAlarm.SetSoundByPath(alarms.alarm.sound.soundPath);


            if (alarms.timer.sound.isPredefined)
                tmpTimer.SetPredeffedSOundsByName(alarms.timer.sound.PredefName);
            else
                tmpTimer.SetSoundByPath(alarms.timer.sound.soundPath);


            if (alarms.waker.sound.isPredefined)
                tmpWaker.SetPredeffedSOundsByName(alarms.waker.sound.PredefName);
            else
                tmpWaker.SetSoundByPath(alarms.waker.sound.soundPath);
            

            //fill predefined
            lst_preDef.Items.AddRange(translatePredefs());

            //select currently picked
            selectCurrent();            
        }

        private void selectCurrent()
        {
            lst_preDef.SelectedIndex = -1;
            Sound currSound = getCurrentlyPickedSound();
            rb_predef.Checked = currSound.isPredefined;
            rb_fromFile.Checked = !currSound.isPredefined;
            if (currSound.isPredefined)
            {
                lbl_currPref.Text = Sound.GetNameOf(currSound.predefSound);
                txt_path.Text = "none";
            }
            else
            {
                lbl_currPref.Text = "none";
                txt_path.Text = currSound.soundPath;
            }
        }

        private Sound getCurrentlyPickedSound()
        {
            if (rb_alarm.Checked)
                return tmpAlarm;
            else if (rb_timer.Checked)
                return tmpTimer;
            else
                return tmpWaker;
        }

        private ListBOxItem[] translatePredefs()
        {
            List<ListBOxItem> listList = new List<ListBOxItem>()  ;
            foreach(System.IO.UnmanagedMemoryStream snd in Sound.getPredefinedSounds())
            {
                ListBOxItem item = new ListBOxItem();
                item.Name = Sound.GetNameOf(snd);
                item.Tag = snd;
                listList.Add(item);
            }
            return listList.ToArray();
        }

        private class ListBOxItem
        {
            public object Tag;
            public string Name;

            public override string ToString()
            {
                return Name;
            }
        }


        private void rb_periodical_CheckedChanged(object sender, EventArgs e)
        {
            selectCurrent();
        }

        private void rb_alarm_CheckedChanged(object sender, EventArgs e)
        {
            selectCurrent();
        }

        private void rb_timer_CheckedChanged(object sender, EventArgs e)
        {
            selectCurrent();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            alarms.alarm.sound = tmpAlarm;
            alarms.timer.sound = tmpTimer;
            alarms.waker.sound = tmpWaker;
            alarms.saveSoundSettings();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            Sound sound = getCurrentlyPickedSound();

            if (sound.isPredefined)
                Amenities.SoundPlayer.PlaySound(sound.predefSound, Volume);
            else
            {
                if (!checkFileExists(sound.soundPath))
                    Amenities.SoundPlayer.PlaySound(sound.soundPath, Volume);
                else
                {
                    MessageBox.Show("Audio file you provided either does not exist or is somewhere else.", "File not found",   MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Pick a sound";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "*.wav";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                rb_fromFile.Checked = true;
                txt_path.Text = fdlg.FileName;
            }
        }

        private void lst_preDef_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lst_preDef.SelectedIndex >= 0)
            {
                getCurrentlyPickedSound().SetPredeffedSOundsByName(lst_preDef.SelectedItem.ToString());
                selectCurrent();
            }
        }

        private void rb_fromFile_CheckedChanged(object sender, EventArgs e)
        {
            getCurrentlyPickedSound().SetSoundByPath(txt_path.Text);
            selectCurrent();
        }

        private bool checkFileExists(string path)
        {
            return System.IO.File.Exists(path);
        }

        private void rb_predef_CheckedChanged(object sender, EventArgs e)
        {
            if(lst_preDef.SelectedItems.Count == 0)
                lst_preDef.SelectedIndex = 0;
            getCurrentlyPickedSound().SetPredeffedSOundsByName(lst_preDef.SelectedItem.ToString());
        }

        private void SoundPicker_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }
    }
}

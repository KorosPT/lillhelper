﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreFunctionality
{
    /// <summary>
    /// Timer triggers every now and then and keeps track of ITiming classes
    /// </summary>
    public class TimeDirector
    {
        List<ITiming> periodicHelper = new List<ITiming>();   //CountDowns and stuff (can be paused)
        List<ITiming> specificHelper = new List<ITiming>();   //AlarmClocks and stuff

        public void addTimer(ITiming timer,bool periodical)
        {
            if (periodical)
                periodicHelper.Add(timer);
            else
                specificHelper.Add(timer);
        }

        /// <summary>
        /// The main running mathod. Infinite loop and sleep until shutdown is initiated
        /// </summary>
        public void Run()
        {
            Logger.LogInfo($"{nameof(TimeDirector)} Starting and timing.");
            while (!Settings.ShutingDown)
            {

                    if(Settings.TimerOn)
                    foreach (ITiming timer in periodicHelper)
                        timer.trigger();

                    foreach (ITiming timer in specificHelper)
                        timer.trigger();

                System.Threading.Thread.Sleep((int)Settings.tickPeriod);
            }
            Logger.LogInfo($"{nameof(TimeDirector)} Stopping.");
        }
    }
}

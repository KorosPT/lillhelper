﻿using Amenities;
using CoreFunctionality.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoreFunctionality
{
    /// <summary>
    /// Main form with controls to the application
    /// </summary>
    public partial class MainControls : Form
    {
        IKeyManaging[] keyManager;
        ITimeManaging[] timeManager;

        public MainControls()
        {
            InitializeComponent();
            this.Icon = Resources.icon1;

            if (!Settings.HookOn)
                btn_stopHook.Text = "Turn on KeyHook";
            else
                btn_stopHook.Text = "Turn off KeyHook";

            if (!Settings.TimerOn)
                btn_stopTime.Text = "Start timers";
            else
                btn_stopTime.Text = "Stop timers";
        }

        /// <summary>
        /// Sets the manager classes so their settings can be accessed
        /// </summary>
        /// <param name="keyMan"></param>
        /// <param name="timeMan"></param>
        public void setManagers(IKeyManaging[] keyMan, ITimeManaging[] timeMan)
        {
            this.keyManager = keyMan;
            this.timeManager = timeMan;

            lbl_shortcuts.Text = "";


            foreach (ITimeManaging timem in timeMan)
            {
                managersList.Items.Add(timem);
            }


            foreach (IKeyManaging keym in keyMan)
            {
                managersList.Items.Add(keym);
                //Show the key combinations
                foreach (Helper helper in keym.getHelpers())
                    lbl_shortcuts.Text += helper.ToString() + "\r\n";
            }


            managersList.SelectedIndex = 0;
        }

        /// <summary>
        /// Stops the applcation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_stopApplication_Click(object sender, EventArgs e)
        {
            Settings.ShutingDown = true;
            Application.Exit();
        }

        /// <summary>
        /// Close window on ESC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainControls_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        /// <summary>
        /// Toggle keyboard hook
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_stopHook_Click(object sender, EventArgs e)
        {
            //toogle keyboard hooks

            Settings.HookOn = !Settings.HookOn;
            if (!Settings.HookOn)
                btn_stopHook.Text = "Turn on KeyHook";
            else
                btn_stopHook.Text = "Turn off KeyHook";
        }

        /// <summary>
        /// Toggle timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_stopTime_Click(object sender, EventArgs e)
        {
            Settings.TimerOn = !Settings.TimerOn;
            if (!Settings.TimerOn)
                btn_stopTime.Text = "Start timers";
            else
                btn_stopTime.Text = "Stop timers";
        }

        /// <summary>
        /// When user clicks one of the managers, puts it's settings panel into panel_settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void managersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = managersList.SelectedIndex;
            string selectedinfo = managersList.SelectedItem.ToString();
            foreach (Control cntrol in panel_settings.Controls)
                cntrol.Dispose();
            panel_settings.Controls.Clear();


            panel_settings.Controls.Add(((IManaging)managersList.SelectedItem).getControlPanel());
            return;
            //foreach(IKeyManaging manag in keyManager)
            //    if(manag.getBriefDescription.Equals)

            //is key manager
            if (index < keyManager.Length)
            {
                UserControl panel = keyManager[index].getControlPanel();
                if (panel != null)
                    panel_settings.Controls.Add(panel);
                return;
            }
            //is time manager
            index -= keyManager.Length;
            if(index < timeManager.Length)
            {
                UserControl panel = timeManager[index].getControlPanel();
                if (panel != null)
                    panel_settings.Controls.Add(panel);
                return;
            }
        }

        private void MainControls_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
    }
}

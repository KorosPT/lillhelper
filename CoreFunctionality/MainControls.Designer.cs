﻿namespace CoreFunctionality
{
    partial class MainControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            foreach (System.Windows.Forms.Control cntrol in this.Controls)
                cntrol.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_stopApplication = new System.Windows.Forms.Button();
            this.btn_stopHook = new System.Windows.Forms.Button();
            this.btn_stopTime = new System.Windows.Forms.Button();
            this.managersList = new System.Windows.Forms.ListBox();
            this.lbl_shortcuts = new System.Windows.Forms.Label();
            this.lbl_shortcutsTitle = new System.Windows.Forms.Label();
            this.panel_settings = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btn_stopApplication
            // 
            this.btn_stopApplication.Location = new System.Drawing.Point(313, 279);
            this.btn_stopApplication.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_stopApplication.Name = "btn_stopApplication";
            this.btn_stopApplication.Size = new System.Drawing.Size(65, 37);
            this.btn_stopApplication.TabIndex = 0;
            this.btn_stopApplication.Text = "Exit my LillHelper";
            this.btn_stopApplication.UseVisualStyleBackColor = true;
            this.btn_stopApplication.Click += new System.EventHandler(this.btn_stopApplication_Click);
            // 
            // btn_stopHook
            // 
            this.btn_stopHook.Location = new System.Drawing.Point(243, 279);
            this.btn_stopHook.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_stopHook.Name = "btn_stopHook";
            this.btn_stopHook.Size = new System.Drawing.Size(65, 37);
            this.btn_stopHook.TabIndex = 1;
            this.btn_stopHook.Text = "Stop key hook";
            this.btn_stopHook.UseVisualStyleBackColor = true;
            this.btn_stopHook.Click += new System.EventHandler(this.btn_stopHook_Click);
            // 
            // btn_stopTime
            // 
            this.btn_stopTime.Location = new System.Drawing.Point(313, 237);
            this.btn_stopTime.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_stopTime.Name = "btn_stopTime";
            this.btn_stopTime.Size = new System.Drawing.Size(65, 37);
            this.btn_stopTime.TabIndex = 2;
            this.btn_stopTime.Text = "Stop timers";
            this.btn_stopTime.UseVisualStyleBackColor = true;
            this.btn_stopTime.Click += new System.EventHandler(this.btn_stopTime_Click);
            // 
            // managersList
            // 
            this.managersList.FormattingEnabled = true;
            this.managersList.Location = new System.Drawing.Point(9, 10);
            this.managersList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.managersList.Name = "managersList";
            this.managersList.Size = new System.Drawing.Size(84, 173);
            this.managersList.TabIndex = 3;
            this.managersList.SelectedIndexChanged += new System.EventHandler(this.managersList_SelectedIndexChanged);
            // 
            // lbl_shortcuts
            // 
            this.lbl_shortcuts.AutoSize = true;
            this.lbl_shortcuts.Location = new System.Drawing.Point(16, 202);
            this.lbl_shortcuts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_shortcuts.Name = "lbl_shortcuts";
            this.lbl_shortcuts.Size = new System.Drawing.Size(35, 13);
            this.lbl_shortcuts.TabIndex = 4;
            this.lbl_shortcuts.Text = "label1";
            // 
            // lbl_shortcutsTitle
            // 
            this.lbl_shortcutsTitle.AutoSize = true;
            this.lbl_shortcutsTitle.Location = new System.Drawing.Point(7, 188);
            this.lbl_shortcutsTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_shortcutsTitle.Name = "lbl_shortcutsTitle";
            this.lbl_shortcutsTitle.Size = new System.Drawing.Size(104, 13);
            this.lbl_shortcutsTitle.TabIndex = 5;
            this.lbl_shortcutsTitle.Text = "Shortcuts registered:";
            // 
            // panel_settings
            // 
            this.panel_settings.Location = new System.Drawing.Point(97, 10);
            this.panel_settings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel_settings.Name = "panel_settings";
            this.panel_settings.Size = new System.Drawing.Size(281, 172);
            this.panel_settings.TabIndex = 6;
            // 
            // MainControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 325);
            this.Controls.Add(this.panel_settings);
            this.Controls.Add(this.lbl_shortcutsTitle);
            this.Controls.Add(this.lbl_shortcuts);
            this.Controls.Add(this.managersList);
            this.Controls.Add(this.btn_stopTime);
            this.Controls.Add(this.btn_stopHook);
            this.Controls.Add(this.btn_stopApplication);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainControls";
            this.Text = "LillHelper - Main controlls";
            this.Shown += new System.EventHandler(this.MainControls_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainControls_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_stopApplication;
        private System.Windows.Forms.Button btn_stopHook;
        private System.Windows.Forms.Button btn_stopTime;
        private System.Windows.Forms.ListBox managersList;
        private System.Windows.Forms.Label lbl_shortcuts;
        private System.Windows.Forms.Label lbl_shortcutsTitle;
        private System.Windows.Forms.Panel panel_settings;
    }
}
﻿using Amenities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoreFunctionality
{
    /// <summary>
    /// Receives key strokes, filters them and launches registered IHelper classes
    /// </summary>
    public class KeyDirector
    {
        /// <summary>
        /// Map from KeyCombo to IHelping
        /// </summary>
        Dictionary<KeyCombo, IHelping> keyToHelper = new Dictionary<KeyCombo, IHelping>();

        /// <summary>
        /// Recently pressed key
        /// For purposes of registering combinations
        /// </summary>
        LinkedList<KeyCombo> recentlyPressedKeys = new LinkedList<KeyCombo>();
        /// <summary>
        /// First unprocessed keyCombo
        /// When null everything is prcessed
        /// </summary>
        LinkedListNode<KeyCombo> currentNode = null;


        /// <summary>
        /// Add new key stroke
        /// </summary>
        /// <param name="keyComb"></param>
        /// <returns>Tuple<Should be supressed, buffer not empty></returns>
        public Tuple<bool, bool> KeyPress(KeyCombo keyComb)
        {
            //Alt + Ctrl + H is a reserved key combination and will always be handeled first
            if (keyComb.Equals(new KeyCombo(Keys.Alt | Keys.Control | Keys.H)))
            {
                launchMainControlForm();
                return new Tuple<bool, bool>(true, runMeBabyOneMoreTime());
            }
            //If control pane is opened or if hooking keys is turned off, don't process the key stroke
            if (!shouldRun())
            {
                return new Tuple<bool, bool>(false, runMeBabyOneMoreTime());
            }

            bool keyCombFires = keyToHelper.ContainsKey(keyComb);
            bool supress = false;

            if (keyCombFires)
            {
                if (keyToHelper[keyComb] != null)
                {
                    //Key stroke has a listener
                    recentlyPressedKeys.AddLast(keyComb);
                    supress = keyToHelper[keyComb].suppressKey(recentlyPressedKeys.Last);

                    Logger.LogInfo($"{nameof(KeyDirector)} Registered " + keyComb.ToString() + " to buffer");
                }
                else
                {
                    //Key stroke just part of a combination. Register and move on
                    supress = false;
                    recentlyPressedKeys.AddLast(keyComb);

                    Logger.LogInfo($"{nameof(KeyDirector)} Registered " + keyComb.ToString() + " to buffer, no reaction");
                }
            }
            else
            {
                if (keyComb.KeyCode != Keys.None && recentlyPressedKeys.Last != null && !recentlyPressedKeys.Last.Value.IsSequenceBreak())
                {
                    keyComb.SetSequenceBreak(true);
                    recentlyPressedKeys.AddLast(keyComb);
                }
            }

            if (currentNode == null)
                currentNode = recentlyPressedKeys.Last;

            return new Tuple<bool, bool>(supress, runMeBabyOneMoreTime());
        }

        /// <summary>
        /// Is there a reason to not listen to keyboard hook?
        /// </summary>
        /// <returns></returns>
        private bool shouldRun()
        {
            if (Application.OpenForms.OfType<MainControls>().Count() > 0)
                return false;

            if (!Settings.HookOn)
                return false;

            return true;
        }

        /// <summary>
        /// Starts a form with LillHelper controls
        /// </summary>
        public void launchMainControlForm()
        {
            if (Application.OpenForms.OfType<MainControls>().Count() > 0)
                return;

            try
            {
                MainControls controls = new MainControls();
                controls.setManagers(CoreDispatcher.keyManagers, CoreDispatcher.timeManagers);
                CoreDispatcher.createNotificationIcon();
                controls.Show();
            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(CoreDispatcher)}: {e.Message} {Environment.NewLine} {e.StackTrace}");
            }
        }

        public void Dispose()
        {
            foreach (IHelping helper in keyToHelper.Values)
                if (helper != null)
                    helper.Dispose();
        }

        /// <summary>
        /// Add new helper
        /// </summary>
        /// <param name="keyComb"></param>
        /// <param name="helper"></param>
        public void AddCombination(KeyCombo keyComb, IHelping helper)
        {
            if ((helper == null && !keyToHelper.ContainsKey(keyComb)) || helper != null)
            {
                keyToHelper[keyComb] = helper;

                if (helper != null)
                    Logger.LogInfo($"{nameof(KeyDirector)} Registered trigger: " + helper.ToString());
                else
                    Logger.LogInfo($"{nameof(KeyDirector)} Registered combination: " + keyComb.ToString());
            }

        }

        /// <summary>
        /// Precess a top key stroke in the buffer
        /// </summary>
        /// <returns></returns>
        public String processBuffer()
        {
            if (currentNode == null) return null;

            LinkedListNode<KeyCombo> fire = currentNode;
            currentNode = currentNode.Next;
            Logger.LogInfo($"{nameof(KeyDirector)} Processing buffer: " + (fire.Value.IsSequenceBreak() ? "SeqenceBreak" : fire.Value.ToString()));

            //KeyCombo is not in any helper. Sequence breaker.
            if (!keyToHelper.ContainsKey(fire.Value))
            {
                if (fire.Value.IsSequenceBreak())
                {
                    Logger.LogInfo($"{nameof(KeyDirector)} Key not part of any combination. Clearing keyCombo buffer.");
                    clearAllPreviousStrokes(fire);
                }
                else
                    Logger.LogError($"{nameof(KeyDirector)} Key not part of any combination, but not set as a sequenceBreak.");

                return null;
            }

            fire.Value.StartCuntDown(); //Start stopwatch in the keyCombo so it expires soon (won't remember all the key strokes from the begining of time)

            //Has no IHelper, so it is not the last keyCombo of any helper. Stop
            if (keyToHelper[fire.Value] == null)
            {
                Logger.LogInfo($"{nameof(KeyDirector)} Dud key processed. Part of key combination.");
                return null;
            }

            clearTimedOutKeyStrokes();

            //Run the listener for the key combination
            bool success = false;
            IHelping currentHelper = keyToHelper[fire.Value];
            try
            {
                success = currentHelper.processKeyStroke(fire);
            }
            catch (Exception e)
            {
                Logger.LogInfo($"{nameof(KeyDirector)} Exception while proccessing: " + currentHelper.ToString());
                Logger.LogInfo($"{nameof(KeyDirector)} " + e.Message.ToString() + e.StackTrace.ToString());
                return null;
            }
            if (!success)
                Logger.LogInfo($"{nameof(KeyDirector)} Problem while proccessing: " + currentHelper.ToString());
            else if (currentHelper.playNotification())
                return currentHelper.GetType() + " triggered.";     //Text for the notification bubble
            return null;
        }

        /// <summary>
        /// Removes old key strokes from recently pressed keys
        /// </summary>
        private void clearTimedOutKeyStrokes()
        {
            while (recentlyPressedKeys.First != null)
            {
                if (recentlyPressedKeys.First.Value.TimedOut((int)Settings.tickPeriod))
                    recentlyPressedKeys.RemoveFirst();
                else
                    return;
            }
        }

        private void clearAllPreviousStrokes(LinkedListNode<KeyCombo> keyCombo)
        {
            while (recentlyPressedKeys.First != null && recentlyPressedKeys.First != keyCombo)
            {
                recentlyPressedKeys.RemoveFirst();
            }
            if (recentlyPressedKeys.First != null)
                recentlyPressedKeys.RemoveFirst();
        }

        /// <summary>
        /// Are there key strokes to process?
        /// </summary>
        /// <returns></returns>
        public bool runMeBabyOneMoreTime()
        {
            return currentNode != null;
        }

    }
}

﻿using System.Runtime.CompilerServices;
using System.Threading;
using Amenities;
using System;
using System.Windows.Forms;
using CoreFunctionality.Properties;
using System.Collections.Generic;
using MessyNote3;

namespace CoreFunctionality
{
    /// <summary>
    /// Handling requests from external sources (connecting KeyDirector and Key Hook, reacting to user's open GUI requests etc...)
    /// </summary>
    public class CoreDispatcher
    {
        /// <summary>
        /// Processes key strokes
        /// </summary>
        public static KeyDirector director { get; set; }
        /// <summary>
        /// Keeps the time related helpers running
        /// </summary>
        public static TimeDirector timer { get; set; }

        /// <summary>
        /// React to key stroke
        /// </summary>
        public static IKeyManaging[] keyManagers;
        /// <summary>
        /// React to passage of time
        /// </summary>
        public static ITimeManaging[] timeManagers;

        /// <summary>
        /// Items in Notification menu
        /// </summary>
        public static List<MenuItem> menuItems;

        /// <summary>
        /// Triggers the KeyDirector.ProcessBuffer
        /// </summary>
        public static AutoResetEvent notifier;

        /// <summary>
        /// Thread running TimeDirector
        /// </summary>
        Thread timeThread;

        public CoreDispatcher(AutoResetEvent waitHandle)
        {
            System.Windows.Forms.Application.EnableVisualStyles();

            Settings.loadSettings();

            notifier = waitHandle;
            director = new KeyDirector();
            timer = new TimeDirector();

            Settings.ShutingDown = false;
            Settings.HookOn = true;
            Settings.TimerOn = true;

            //Create the classes that react to things
            keyManagers = new IKeyManaging[] { new MessyNote3.MessyNote3(), new JapIn.JapIn(), new TextMan.TextMan() };
            timeManagers = new ITimeManaging[] { new AlarmingAlarms.AlarmingAlarms() };

            menuItems = new List<MenuItem>();

            //Map things that react  to stuff to Directors getMenuButtons
            foreach (IKeyManaging keyMan in keyManagers)
            {
                menuItems.AddRange(keyMan.getMenuButtons());
                foreach (IHelping helper in keyMan.getHelpers())
                {
                    for (int i = 0; i < helper.getKeyBinding().Length; i++)
                        if (i < helper.getKeyBinding().Length - 1)
                            director.AddCombination(helper.getKeyBinding()[i], null);   //Record because it is a part of key combinations, but does nothing (Ctrl + X && Ctrl + H = Ctrl + X does nothing, but has to be recorded)
                        else
                            director.AddCombination(helper.getKeyBinding()[i], helper);
                }
            }

            foreach (ITimeManaging timeMan in timeManagers)
            {
                menuItems.AddRange(timeMan.getMenuButtons());
                foreach (ITiming alarm in timeMan.getTimers())
                {
                    timer.addTimer(alarm, alarm.isPeriodical());
                }
            }


            createNotificationIcon();
        }

        public void Run()
        {
            try
            {
                Logger.LogInfo($"{nameof(CoreDispatcher)} Starting and waiting.");
                String triggeredHelper = "";
                //Start timeDirector
                timeThread = new Thread((new ThreadStart(timer.Run)));
                timeThread.Start();

                //Start keyDirector
                notifier.WaitOne();
                while (!Settings.ShutingDown)
                {
                    triggeredHelper = director.processBuffer(); //check the buffer for processable key strokes

                    if (!String.IsNullOrEmpty(triggeredHelper)) //if keyDirector turned something on, make a bubble about it
                        NotificationAreaIcon.notify(triggeredHelper);

                    //Is there something more in the buffer?
                    if (director.runMeBabyOneMoreTime())
                        notifier.Set();

                    //Wait till the buffer has something
                    notifier.WaitOne();
                }
                Dispose();

            }
            catch (Exception e)
            {
                Logger.LogError($"{nameof(CoreDispatcher)}: {e.Message} {Environment.NewLine} {e.StackTrace}");
            }

            Logger.LogInfo($"{nameof(CoreDispatcher)} DED");
        }

        /// <summary>
        /// Turning off
        /// </summary>
        private void Dispose()
        {
            timeThread.Join();  //Time director starts shutting down the same time as keyDirector, but still have to wait for it
            director.Dispose();
        }

        private static Thread staThread;
        /// <summary>
        /// Creates a notification icon in notification area
        /// </summary>
        public static void createNotificationIcon()
        {
            if (staThread != null)
                return;
            if (staThread != null && staThread.IsAlive)
                return;

            staThread = new Thread(
            delegate ()
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Application.Run(new NotificationAreaIcon(director, menuItems.ToArray()));
            });
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
        }
    }

    /// <summary>
    /// Notification icon
    /// Reacts to double click and right click
    /// </summary>
    public class NotificationAreaIcon : ApplicationContext
    {
        private static NotifyIcon trayIcon;
        private KeyDirector director;

        public NotificationAreaIcon(KeyDirector director, MenuItem[] items)
        {
            this.director = director;
            // Initialize Tray Icon
            trayIcon = new NotifyIcon()
            {
                Icon = Resources.icon1,
                Visible = true
            };

            trayIcon.ContextMenu = new ContextMenu();
            //(new MenuItem[] {
            //    ,
            //    add items,

            //};

            trayIcon.ContextMenu.MenuItems.Add(new MenuItem("Open", openControlls));
            if (items.Length != 0)
            {
                trayIcon.ContextMenu.MenuItems.Add(new MenuItem("-"));
                trayIcon.ContextMenu.MenuItems.AddRange(items);
                trayIcon.ContextMenu.MenuItems.Add(new MenuItem("-"));
            }
            trayIcon.ContextMenu.MenuItems.Add(new MenuItem("Exit", Exit));

            trayIcon.DoubleClick += openControlls;
        }

        void openControlls(object sender, EventArgs e)
        {
            director.launchMainControlForm();
        }

        void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            trayIcon.Visible = false;
            Settings.ShutingDown = true;

            Application.Exit();
        }

        /// <summary>
        /// Create a bubble with text
        /// </summary>
        /// <param name="text"></param>
        public static void notify(String text)
        {
            if (trayIcon == null) return;
            trayIcon.BalloonTipText = text;
            trayIcon.ShowBalloonTip(0);
        }

    }
}
